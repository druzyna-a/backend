<script type="text/javascript">
function add_comment(table){
    var comment =  document.getElementById('replyFormComment').value;
    <?php $date = date("H:i d.m.Y"); ?>
    var date =  "<?php echo $date; ?>";
    var table =  table;
    var id =  <?php echo $this->uri->segment(3); ?>;
      if(comment !="" ){
       $.ajax({  
                         url:"<?php echo base_url(); ?>comments/add_comment/"+table+"/"+id+"",   
                         method:"POST",   
                         data:{comment:comment,date:date},
                         cache: false,  
                         success:function(data)  //success po wysłaniu
                         {  
                            document.getElementById('replyFormComment').value = "";
                            document.getElementById('success').innerHTML= "<span id='success_text' class='text-success animated fadeIn' data-wow-duration='5s'>Dodałeś komenatarz</span>";

                            setTimeout(function(){ 
                               $( "#success_text" ).fadeOut("slow");
                             }, 3000);
                            var new_com = '<div class="media d-block d-md-flex mt-3"><div class="media-body text-center text-md-left ml-md-3 ml-0"><h5 class="mt-0 font-weight-bold"><?php echo $_SESSION['name']; ?> <small><?php echo $date; ?></small></h5>'+comment+'</div></div>';
                            document.getElementById('comments').innerHTML += new_com;


                         },        
                    }); 
     }else{
          document.getElementById('success').innerHTML= "<span id='success_text' class='text-danger animated fadeIn' data-wow-duration='5s'>Uzupełnij pole!</span>";
          setTimeout(function(){$( "#success_text" ).fadeOut("slow"); }, 3000);
     }
  }
</script>
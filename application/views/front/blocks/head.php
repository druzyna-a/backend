<!DOCTYPE html>
<html lang="en">

<head style="overflow-x: hidden;">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php if($this->uri->segment(1) == 'blog_wpis'): ?>
        <title><?php echo $entry->meta_title; ?></title>
    <?php else: ?>
    <title><?php echo $settings->meta_title; ?></title>
    <?php endif; ?>
    <?php if($this->uri->segment(1) == 'blog_wpis'): ?>
        <meta name="description" content="<?php echo $entry->meta_description; ?>">
    <?php else: ?>
    <meta name="description" content="<?php echo $settings->description; ?>">
    <?php endif; ?>
    <?php if($this->uri->segment(1) == 'blog_wpis'): ?>
    <meta name="keywords" content="<?php echo $entry->keywords; ?>">
    <?php else: ?>
    <meta name="keywords" content="<?php echo $settings->keywords; ?>">
    <?php endif; ?>
    <link rel="icon" href="<?php echo base_url(); ?>uploads/<?php echo $settings->logo; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen:400,700&display=swap&subset=latin-ext" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/css/owl.carousel.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <?php if($this->uri->segment(1) == ''): ?>
        <link href="<?php echo base_url(); ?>assets/front/css/header-style.css" rel="stylesheet">
    <?php else: ?>
        <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
    <?php endif; ?>
    
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js?render=6LcdRMwUAAAAABCvy21DWoEhiXG0cZxpapcG1bss"></script>

</head>

<body style="overflow-x: hidden;">

<?php if(isset($_SESSION['flashdata'])): ?>
<div id="hideInfo" class="info-box bg-danger"><?php echo $_SESSION['flashdata']; ?></div>
<?php elseif(isset($_SESSION['flashdata_success'])): ?>
<div id="hideInfo" class="info-box bg-success"><?php echo $_SESSION['flashdata_success']; ?></div>
<?php endif; ?>
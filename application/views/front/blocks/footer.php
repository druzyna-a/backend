<!--Footer-->

<footer class="page-footer  text-center text-md-left">

    <!--Copyright-->
    <div class="footer-copyright py-3 text-center elegant-color" style="margin-top: 400px;">
        <div class="container-fluid">
            © 2019 Copyright

        </div>
    </div>
    <!--/.Copyright-->

</footer>

<!--/.Footer-->


<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.min.js"></script>
<!-- DataTable JavaScript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/addons/datatables.min.js"></script>

<script src="https://cdn.tiny.cloud/1/no6be3fmvmbx2b6q28pch51gwxb2vourdipy9rtbpv78od26/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="<?php echo base_url(); ?>assets/front/js/owl.carousel.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/deletePhoto.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/loadPhoto.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/scripts.js"></script> 
<?php if($this->uri->segment(1) == 'sklep'): ?>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/search_books_categories.js"></script> 
    <script type="text/javascript">
      search_books_categories();
    </script>
<?php endif; ?>

<script type="text/javascript">
      moveBackground();
</script>

<script type="text/javascript">

function changeLang(lang){
$.ajax({
url:"<?php echo base_url(); ?>home/change/"+lang+"",
         success: function () {
            location.reload();
        }
});
}

</script>
<?php if($_SESSION['lang'] != 'pl'): ?>
  <script type="text/javascript">
    function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'pl', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
    jQuery('.lang-select').click(function() {
    var theLang = jQuery(this).attr('data-lang');
    jQuery('.goog-te-combo').val(theLang);
    window.location = jQuery(this).attr('href');
    location.reload();
    });
  </script>
<?php endif; ?>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


</body>

</html>
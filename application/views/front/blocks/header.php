<header>

    <!--Navbar-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
        <div class="container">

            <a href="<?php echo base_url(); ?>"><span class="logo-background mt-2 mr-2" style="background-image: url('<?php echo base_url(); ?>assets/front/img/logo.png');"></span></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse mb-2 mt-2" id="navbarSupportedContent">

                <ul class="navbar-nav mr-auto ">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>blog">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>o_nas">O nas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>sklep">Sklep</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>ksiazki_uzytkownikow">Książki użytkowników</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>kontakt">Kontakt</a>
                    </li>
                    <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-flag"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-default"
                    aria-labelledby="navbarDropdownMenuLink-333">
                    <a class="dropdown-item notranslate" href="#googtrans(pl|pl)" data-lang="pl" onclick="changeLang('pl');">PL</a>
                    <a class="dropdown-item notranslate" href="#googtrans(pl|en)" data-lang="en" onclick="changeLang('en');">EN</a>
                    </div>
                </li>
                    
                </ul>

                <!-- <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <form class="form-inline">
                            <div class="md-form my-0">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search"
                                       aria-label="Search">
                            </div>
                        </form>
                    </li>

                    <li class="nav-item">

                    </li>

                </ul>-->
                <?php if(isset($_SESSION['login_client']) && $_SESSION['login_client'] == true): ?>
                    <ul class="navbar-nav dropdown">
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> <span
                                class="clearfix d-none d-sm-inline-block">Konto</span></a>
                        <div class="dropdown-menu "
                             aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item " href="<?php echo base_url(); ?>profil">Edytuj profil</a>
                            <?php if(@$_SESSION['premium'] == '1'): ?>
                            <a class="dropdown-item " href="<?php echo base_url(); ?>twoje_wpisy">Zobacz swoje wpisy</a>
                            <?php endif; ?>
                            <a class="dropdown-item " href="<?php echo base_url(); ?>twoje_ksiazki">Zobacz napisane przez Ciebie książki</a>
                            <a class="dropdown-item " href="<?php echo base_url(); ?>biblioteka">Twoja biblioteka</a>
                            <a class="dropdown-item " href="<?php echo base_url(); ?>klienci/logout">Wyloguj</a>

                        </div>
                    </li>
                </ul>
                <?php else: ?>

                <button id="modalActivate" class="btn btn-rounded  light-blue accent-4 mt-1" data-toggle="modal"
                        data-target="#modalLoginForm">LOGOWANIE
                </button>
                <?php endif;?>
                <a href="<?php echo base_url(); ?>home/cart" class="icons-sm so-ic">
                    <i class="fas fa-shopping-cart white-text ml-md-3"></i>
                    <?php if(!empty($this->cart->contents())): ?>
                    <span class="badge badge-primary"><?= count($this->cart->contents()); ?></span>
                    <?php endif; ?>
                </a>

            
            </div>

        </div>
    </nav>
    <!--/Navbar-->

    <!--Modal Login/register-->

    <div class="modal fade " id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog cascading-modal" role="document">
            <div class="modal-content  ">

                <!--login-->
                <div class="modal-c-tabs elegant-color">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs md-tabs tabs-2 blue-gradient darken-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panelLogin" role="tab"><i
                                    class="fas fa-user mr-1"></i>
                                Logowanie</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panelRegister" role="tab"><i
                                    class="fas fa-user-plus mr-1"></i>
                                Rejestracja</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panelPassword" role="tab"><i
                                    class="fas fa-unlock mr-1"></i>
                                Resetowanie hasła</a>
                        </li>
                    </ul>

                    <!-- Tab panels -->
                    <div class="tab-content ">

                        <!--Panel login-->
                        <div class="tab-pane fade in show active " id="panelLogin" role="tabpanel">
                        <form action="<?php echo base_url(); ?>klienci/login" method="post" class="modal-body mb-1">
                            <div class="modal-body mb-1">
                                <div class="md-form form-sm mb-2 ">
                                    <i class="fas fa-envelope prefix"></i>
                                    <input type="email" id="emailInputLogin"
                                           class="form-control form-control-sm validate" name="login">
                                    <label data-error="wrong" data-success="right" for="emailInputLogin">E-mail</label>
                                </div>

                                <div class="md-form form-sm mb-2">
                                    <i class="fas fa-lock prefix"></i>
                                    <input type="password" id="passwordInputLogin"
                                           class="form-control form-control-sm validate" name="password">
                                    <label data-error="wrong" data-success="right"
                                           for="passwordInputLogin">Hasło</label>
                                </div>

                                <div class="text-center mb-2">
                                    <button class="btn blue-gradient btn-lg">Zaloguj</button>
                                </div>
                            </div>
                        </form>
                        </div>

                        <!--Panel password reset-->
                        <div class="tab-pane fade" id="panelPassword" role="tabpanel">
                        <form action="<?php echo base_url(); ?>klienci/reset_pass" method="post" class="modal-body mb-1">
                            <div class="modal-body mb-1">
                                <div class="md-form form-sm mb-2 ">
                                    <i class="fas fa-envelope prefix"></i>
                                    <input type="email" id="emailInputLogin"
                                           class="form-control form-control-sm validate" name="email">
                                    <label data-error="wrong" data-success="right" for="emailInputLogin">E-mail</label>
                                </div>

                            
                                <div class="text-center mb-2">
                                    <button class="btn blue-gradient btn-lg">Wyślij</button>
                                </div>
                            </div>
                        </form>
                        </div>

                        <!-- panel register -->

                        <div class="tab-pane fade" id="panelRegister" role="tabpanel">

                            <!--Body-->
                            <form action="<?php echo base_url(); ?>klienci/register" method="post" class="modal-body mb-1">
                            <div class="modal-body">

                                <div class="md-form form-sm mb-2">
                                    <i class="fas fa-envelope prefix"></i>
                                    <input type="email" id="emailInputRegister" name="email"
                                           class="form-control form-control-sm validate">
                                    <label data-error="wrong" data-success="right"
                                           for="emailInputRegister">E-mail</label>
                                </div>

                                <div class="md-form form-sm mb-5">
                                    <i class="fas fa-lock prefix"></i>
                                    <input type="password" id="passwordInputRegister" name="password"
                                           class="form-control form-control-sm validate">
                                    <label data-error="wrong" data-success="right"
                                           for="passwordInputRegister">Hasło</label>
                                </div>

                                <div class="md-form form-sm mb-2">
                                    <i class="fas fa-lock prefix"></i>
                                    <input type="password" id="passwordRepeatInputRegister" name="confirm_password"
                                           class="form-control form-control-sm validate">
                                    <label data-error="wrong" data-success="right" for="passwordRepeatInputRegister">Powtórz
                                        hasło</label>
                                </div>

                                <div class="text-center mb-3">
                                    <button class="btn blue-gradient">zarejestruj <i class="fas fa-sign-in ml-1"></i>
                                    </button>
                                </div>

                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


</header>
<img src="" style="display: none;">
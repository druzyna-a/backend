<main>

    <div class="container pt-5 mt-5">

        <section class="text-center team-section wow fadeIn" data-wow-delay="0.3s">

            <!--Grid row-->
            <div class="row text-center">

                <!--Grid column-->
                <div class="col-md-12 mt-5 mb-5">

                    <!--Section: edit profile-->
                    <section class="wow fadeIn" data-wow-delay="0.5s">

                        <h2 class="h1-responsive font-weight-bold text-center ">Profil użytkownika: <?php echo @$user_data->first_name; ?> <?php echo @$user_data->last_name; ?></h2>
                        <hr class=" hr-dark ">

                        <!--Tab panels -->
                        <div class="tab-content">

                            <!--personal data-->
                            <div class="tab-pane fade show active" id="PersonalData" role="tabpanel">
                                <br>
                                <div class="row text-center">
                                <div class="col-md-6 mt-5">
                                    <div class="file-field">
                                            <div class="mb-4">

                                            <?php if(@$user_data->photo != '') {
                                            echo '<img class="rounded-circle z-depth-1 img-fluid" src="' . base_url() . 'uploads/' . @$user_data->photo . '" width=200px>';
                                            } else {
                                             echo '<img class="rounded-circle z-depth-1 card-img-100 img-fluid" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg" alt="ebookapp">';
                                            } ?>
                                                
                                                
                                            </div>
                                                
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-5">
                                        <div class="md-form">
                                        <input type="text" id="name" class="form-control mb-4"
                                               placeholder="imie" value="<?php echo @$user_data->first_name; ?>" readonly>
                                        </div>
                                        <div class="md-form">
                                        <input type="text" id="surname" class="form-control mb-4"
                                               placeholder="nazwisko" value="<?php echo @$user_data->last_name; ?>" readonly>
                                        </div>
                                        <div class="md-form">
                                        <input type="email" id="email" class="form-control mb-4 validate"
                                               placeholder="email" value="<?php echo @$user_data->email; ?>" readonly>
                                        </div>
                                        <!--Textarea with icon prefix-->
                                        <div class="md-form">
                                            <textarea id="form10" class="md-textarea form-control" rows="3" readonly><?php echo @$user_data->description; ?></textarea>
                                            <label for="form10">Opis profilu</label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </div>
                            <!--/personal data-->


                        </div>


                    </section>
                    <!--/Section: edit profile-->

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </section>
        <!--/Section: about me-->


    </div>

    <div class="container">


  <!-- Section: Block Content -->
  <section class="section text-center">

  <h2 class="font-weight-bold text-center h1 my-5 pt-5 pb-3 ">Książki użytkownika <?php echo @$user_data->first_name; ?> <?php echo @$user_data->last_name; ?></h2>
  <hr class=" hr-dark ">

    <!-- Grid row -->
    <div class="row mt-5">

      <?php $i=0; foreach (array_reverse($user_ebooks) as $value):?>
      <!-- Grid column -->
      <div class="col-lg-4 col-md-12 mb-4">

        <div class="view overlay rounded z-depth-1">
          <img src="<?php echo images().'/'.$value->photo; ?>" class="img-fluid" alt="Sample project image" style="width: 100%">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>

        <div class="px-3 pt-3 mx-1 mt-1 pb-0">
          <h4 class="font-weight-bold mt-1 mb-3"><?php echo $value->book_title ?></h4>
          <p class="text-muted"><?php echo substr($value->description, 0, 50) ?>...</p>
          <a href="<?php echo base_url(); ?>home/user_book/<?php echo $value->id; ?>/<?php echo slug($value->title); ?>" class="btn btn-primary"><i class="fas fa-clone left"></i> Czytaj więcej</a>
        </div>

      </div>
      <!-- Grid column -->
      <?php $i++; endforeach; ?>
      

    </div>
    <!-- Grid row -->

  </section>
  <!-- Section: Block Content -->


</div>

</main>
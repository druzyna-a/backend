<section>
		<div class="container pt-5 mt-5">

            <div class="row text-center">

            <div class="col-md-12 mt-5 mb-5">

            <h2 class="h1-responsive font-weight-bold text-center ">Twoje książki</h2>
            <hr class=" hr-dark ">


            </div>

            </div>

			<!-- Table with panel -->
			<div class="card card-cascade narrower mb-5">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <div class="md-form my-0">
					<!-- <input type="text" id="searchInput" class="form-control"  onkeyup="searchFiltr(0)" placeholder="Wpisz nazwę..." title="Wpisz nazwę"> -->
					<a href="" class="white-text mx-3">Dodaj nową książkę</a>
			    </div>

			    

			    <div class="text-right" style="width: 181px;">
			    	<a href="<?php echo base_url(); ?>dodaj_ksiazke/form/insert">
				      <button type="button" class="btn btn-outline-white btn-rounded btn-hover-alt btn-sm px-2">
				        <i class="fas fa-plus mt-0"></i>
				      </button>
			      	</a>
			    </div>

			  </div>
			  <!--/Card image-->

			  <div class="px-4 pb-3">

			    <div class="table-wrapper ">
				    <span id="refreshTable">
				      <!--Table-->
				      <table id="filtrableTable" class="table table-hover table-responsive mb-0">

				        <!--Table head-->
				        <thead>
				          <tr>
                              <th ></th>
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 30%;">
				              Tytuł
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableData(1)" style="width: 20%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th style="width: 30%;"></th>
				          </tr>
				        </thead>
				        <!--Table head-->

				        <!--Table body-->
				        <tbody>
						  	<?php foreach (array_reverse($rows) as $value): ?>
						  		<tr id="<?php echo $value->id?>">
						            <td class="align-middle">
						            	<img src="<?php echo base_url(); ?>uploads/<?php echo $value->photo; ?>" class="img-fluid"><br>
						            </td>
                                    <td class="align-middle"><?php echo $value->book_title; ?></td>
						            <td class="align-middle"><?php echo $value->created; ?></td>
						            <td class="align-middle text-right">
									  <a href="<?php echo base_url(); ?>rozdzialy/<?php echo $value->id; ?>/<?php echo slug($value->book_title); ?>" title="Pisz">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									      <i class="fas fa-book mt-0"></i>
									    </button>
									  </a>
						              <a href="<?php echo base_url(); ?>edytuj_ksiazke/form/update/<?php echo $value->id; ?>/<?php echo slug($value->book_title); ?>" title="Edycja">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									      <i class="fas fa-pencil-alt mt-0"></i>
									    </button>
									  </a>
								      <a href="<?php echo base_url(); ?>home/delete/add_ebook/<?php echo $value->id; ?>" onclick="return confirm('Czy na pewno chcesz usunąć <?php echo $value->title; ?>? #<?php echo $value->id; ?>')" >
									      <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-trash-alt mt-0"></i>
									      </button>
								      </a>
								  	</td>
				            	</tr>
							<?php endforeach; ?>
							

				        </tbody>
				        <!--Table body-->
				      </table>
				      <!--Table-->
			      </span>
			    </div>

			  </div>

			</div>
			<!-- Table with panel -->
		</div>
	</section>
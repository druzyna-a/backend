<!--Main Layout-->
<main>

    <div class="container pt-5 mt-5 mb-5">

        <div class="card">
            <div class="card-body">

                <!-- Shopping Cart table -->
                <div class="table-responsive">

                    <table class="table product-table">

                        <!-- Table head -->
                        <thead class="mdb-color lighten-5">
                        <tr>
                            <th></th>
                            <th class="font-weight-bold">
                                <strong>Ebook</strong>
                            </th>

                            <th></th>
                            <th></th>
                            <th class="font-weight-bold">
                                <strong>Cena</strong>
                            </th>
                            <th></th>

                        </tr>
                        </thead>
                        <!-- /.Table head -->

                        <!-- Table body -->
                        <tbody>

                        <?php $suma = 0;
                        foreach ($this->cart->contents() as $value): ?>
                            <!-- First row -->
                            <tr>
                                <th scope="row">
                                    <img src="<?php echo base_url(); ?>uploads/<?= $value['photo']; ?>"
                                         alt="" class="img-fluid z-depth-0">
                                </th>
                                <td>
                                    <h5 class="mt-3">
                                        <strong><?= $value['name']; ?></strong>
                                    </h5>
                                    <p class="text-muted"><?= $value['author']; ?></p>
                                </td>
                                <td></td>
                                <td></td>
                                <td class="font-weight-bold">
                                    <strong><?= str_replace(',', ' ', number_format($value['price'])); ?> zł</strong>
                                </td>
                                <td>
                                    <a href="<?= base_url('cart/remove_good/' . $value['rowid']); ?>"
                                       class="btn btn-sm btn-primary" data-toggle="tooltip"
                                       data-placement="top"
                                       title="Remove item">X
                                    </a>
                                </td>
                            </tr>
                            <!-- /.First row -->
                            <?php $suma += $value['price']; endforeach ?>

                        <!-- Fourth row -->
                        <tr>
                            <td colspan="4"></td>
                            <td>
                                <h4 class="mt-2">
                                    <strong>Suma koszyka: </strong>
                                </h4>
                            </td>
                            <td class="text-right">
                                <h4 class="mt-2">
                                    <strong><?= str_replace(',', ' ', number_format($suma)); ?> zł</strong>
                                </h4>
                            </td>

                        </tr>
                        <!-- Fourth row -->

                        </tbody>
                        <!-- /.Table body -->

                    </table>

                </div>
                <!-- /.Shopping Cart table -->

                <div class="text-right mr-5">
                    <a href="<?= base_url('home/payment'); ?>" class="  btn btn-primary btn-rounded ">dokończ zakupy
                        <i class="fas fa-angle-right right"></i>
                    </a>
                </div>
            </div>

        </div>

    </div>

</main>
<!--Main Layout-->
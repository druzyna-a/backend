<main>

    <div class="container pt-5 mt-5">


        <!--Grid row-->
        <div class="row ">

            <!--Grid column-->
            <div class="col-md-12 mt-5 mb-5">
                <h2 class="font-weight-bold dark-grey-text text-center">Dodawanie książki</h2>
                <hr>
            </div>
            <div class="col-md-6">
                <form class="md-form">
                    <div class="file-field">
                        <a class="btn-floating blue-gradient mt-0 float-left">
                            <i class="fas fa-images" aria-hidden="true"></i>
                            <input type="file">
                        </a>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Wgraj okładkę książki">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 mb-5">
                <form class="md-form">
                    <div class="file-field">
                        <a class="btn-floating blue-gradient mt-0 float-left">
                            <i class="fas fa-file-upload" aria-hidden="true"></i>
                            <input type="file">
                        </a>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Wgraj książkę">
                        </div>
                    </div>
                </form>
            </div>

        </div>

        <div class="row justify-content-md-center ">
            <div class="col-md-7">
                <form action="<?php echo base_url(); ?>home/action/insert/add_ebook" method="post">
                <div class="md-form mb-5 mt-5 ">
                    <input type="text" id="title" name="book_title"class="form-control">
                    <label for="title">Tytuł książki</label>
                </div>
                <div class="md-form mb-5 mt-5 ">
                    <input type="text" id="tags" name="tags"class="form-control" >
                    <label for="tags">Tagi</label>
                </div>





                <textarea name="description" class="summernote" ></textarea>
                <button id="SaveChanges"
                        class="btn btn btn-primary pr-5 pl-5 float-right" type="submit">
                    Dodaj
                </button>
                </form>
            </div>

        </div>


    </div>

</main>

<div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">

<ol class="carousel-indicators">
    <?php $i=0; foreach ($slider as $value):?>
      <li data-target="#carousel-example-1z" data-slide-to="<?php echo $i ?>" class="<?php echo ($i == 0 ? 'active' : '') ?>"></li>
  <?php $i++; endforeach; ?>
</ol>

<div class="carousel-inner" role="listbox">

    <?php $i=0; foreach ($slider as $value):?>
      <div class="carousel-item <?php echo ($i == 0 ? 'active' : '') ?>">
        <div class="view moving__photo" style="background-image: url('<?php echo images().'/'.$value->photo ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">

          <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

            <div class="text-center white-text mx-5 wow fadeIn text__box">

                <h1><?php echo $value->title ?></h1>
                <h4><?php echo $value->subtitle ?></h4>
            </div>

          </div>

        </div>
      </div>

  <?php $i++; endforeach; ?>


</div>

</div>
<main>
        <section class="container-fluid">
            <h2 class="h1-responsive font-weight-bold text-center  mt-5">O nas</h2>
            <hr width="150" class="mb-5">
            <div class="row">
            <?php $i=0; foreach ($about as $value): $i++; if($i%2!=0): ?>
            <div class="col-12 bg-photo d-lg-none" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>
            <div class="col-lg-6 bg-muted first_section">
             <h1 class="mb-4 wow fadeInLeft"><?php echo $value->title; ?></h1>
                <div class="font-regular text-muted mb-0 wow fadeInLeft"><?php echo $value->description; ?></div>
            </div>
            <div class="col-lg-6 bg-photo d-none d-lg-block" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>

            <?php else: ?>

            <div class="col-lg-6 bg-photo d-none d-lg-block" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>
            <div class="col-12 bg-photo d-lg-none" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>
            <div class="col-lg-6 first_section">
                <h1 class="mb-4 wow fadeInRight"><?php echo $value->title; ?></h1>
                <div class="font-regular text-muted mb-0 wow fadeInRight"><?php echo $value->description; ?></div>
            </div>
            <?php endif; endforeach ?>
            </div>
        </section>

    <section id="book_carousel">
        <div class="row d-flex justify-content-center">

            <div class="col-md-10 mt-5">
                <h2 class="h1-responsive font-weight-bold text-center pt-2">Polecane</h2>
                <hr width="150">
                <div class="owl-carousel owl-theme pt-5">
                    <?php $i=0; foreach ($books as $value):?>
                   
                    <div class="item">
                        <!--Card-->
                            
                            <div class="card card-cascade card-ecommerce">

                                <!--Card image-->
                                <div class="view view-cascade overlay bg-photo-card" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>">
                                    
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                                <!--/.Card image-->

                                <!--Card content-->
                                <div class="card-body card-body-cascade text-center">
                                    <a href="<?php echo base_url(); ?>ksiazka/<?php echo $value->id; ?>/<?php echo slug($value->title); ?>">
                                    <h5><span class="text-dark font-italic" href=""><?php echo $value->author ?></span></h5>
                                    <h4 class="card-title"><strong><?php echo $value->title ?></strong></h4>
                                    </a>
                                    <!--Description-->
                                    <p class="card-text"><?php echo substr($value->description, 0, 50) ?>...
                                    </p>

                                    <!--Card footer-->
                                    <div class="card-footer">
                                        <span class="float-left"><?php echo $value->price ?> zł </span>
                                        <?php if(isset($_SESSION['login_client']) && $_SESSION['login_client'] == true): ?>
                                        <?php $bookInCart = false;
                                        foreach ($this->cart->contents() as $cart) {
                                            if($cart['book_id'] == $value->id) {
                                                $bookInCart = true;
                                            }
                                        } ?>
                                        <?php if($bookInCart == false): ?>
                                            <a class="icons-sm so-ic float-right" href="<?= base_url('cart/add_good/'.$value->id); ?>">
                                                <i class="fas fa-cart-plus text-dark"></i>
                                            </a>
                                        <?php else: ?>
                                            <a class="icons-sm so-ic float-right">
                                                <i class="fas fa-check"></i>
                                            </a>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                
                                </div>
                            </div>
                            

                    </div>
                    
                    <?php $i++; endforeach; ?>



                </div>
                <br>
                <br>

            </div>
        </div>
    </section>

        <div class="container mt-5">    
        <!-- premium  -->
        <!-- Section: Premium -->
        <section class="text-center my-5">
            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold text-center mb-2 mt-1">Zostań użytkownikiem premium</h2>
            <hr>
            <!-- Section description -->

            <!-- Grid row -->
            <div class="row mt-5">

                <!-- Grid column -->
                <div class="col-lg-6 col-md-12 mb-lg-0 mb-4 ">

                    <!-- Card -->
                    <div class="card">

                        <!-- Content -->
                        <div class="card-body main_page_card">

                            <!-- Offer -->
                            <h4 class="mb-4 font-weight-bold"><?php echo $normal_user->title ?></h4>
                            <hr>
                            <div class=" justify-content-center">
                            <?php echo $normal_user->description ?>
                            </div>
                            <hr>
                            <!-- Price -->
                            <h2 class="font-weight-bold my-4"><?php echo $normal_user->price ?></h2>


                        </div>
                        <!-- Content -->

                    </div>
                    <!-- Card -->

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-6 col-md-6 mb-md-0 mb-4">

                    <!-- Card -->
                    <div class="card blue-gradient">

                        <!-- Content -->
                        <div class="card-body white-text main_page_card">

                            <!-- Offer -->
                            <h4 class="mb-4 font-weight-bold"><?php echo $premium_user->title ?></h4>
                            <hr>
                            <div class=" justify-content-center">
                            <?php echo $premium_user->description ?>
                            </div>
                            <hr>
                            <!--Price -->
                            <h2 class="font-weight-bold my-3"><?php echo $premium_user->price ?></h2>


                        </div>
                        <!-- Content -->

                    </div>
                    <!-- Card -->

                </div>
                <!-- Grid column -->

            </div>
            <?php if(@$_SESSION['premium'] == '0'): ?>
            <!-- Grid row -->
            <?php if(@$_SESSION['login_client'] && $_SESSION['login_client'] == true): ?>
                <form method="post" action="<?= base_url('premium/platnosc'); ?>">
                  <input type="hidden" id="user_id" name="user_id" class="form-control" value="<?= $_SESSION['id']; ?>" required="">
                  <input type="hidden" id="p24_email" name="p24_email" class="form-control" value="<?= $_SESSION['email']; ?>" required="">
                  <input type="hidden" id="p24_address" name="p24_address" class="form-control" value="PWSZ" required="">
                  <input type="hidden" id="p24_zip" name="p24_zip" class="form-control" value="59-220" required="">
                  <input type="hidden" name="env" value="1" />
                  <input type="hidden" name="name" value="Premium" />
                  <input type="hidden" name="qty" value="1" />
                  <input type="hidden" name="price" value="5000" />
                  <input type="hidden" name="redirect" value="off" />
                  <input type="hidden" name="salt" value="dc20fa75df8d435c" />
                  <input type="hidden" name="p24_merchant_id" value="24816" />
                  <input type="hidden" name="p24_pos_id" value="24816" />
                  <input type="hidden" name="p24_sign" value="<?php echo md5(session_id().'|24816|'. (50 * 100) .'|PLN|dc20fa75df8d435c'); ?>" />
                  <input type="hidden" name="p24_session_id" value="<?php echo session_id(); ?>" />
                  <input type="hidden" name="p24_amount" value="<?php echo floor(50 * 100); ?>" />
                  <input type="hidden" name="p24_currency" value="PLN" />
                  <input type="hidden" name="p24_url_status" value="<?php echo base_url(); ?>premium/status/<?= md5($_SESSION['email'] . '|' . $_SESSION['id']); ?>" />
                  <input type="hidden" name="p24_url_return" value="<?php echo base_url(); ?>premium/podsumowanie?ok=1" />
                  <input type="hidden" name="p24_api_version" value="3.2" />
                  <input type="hidden" name="p24_quantity_1" value="1" />
                  <input type="hidden" style="width:250px" name="p24_wait_for_result" value="0" />
            <button class="btn btn-outline-blue btn-lg btn-rounded mt-5" type="submit">Kup teraz
            </button>
            </form>
            <?php else: ?>
            <button id="modalActivatepernament" class="btn btn-outline-blue btn-lg btn-rounded mt-5"
                    data-toggle="modal"
                    data-target="#modalLoginForm">Zaloguj się
            </button>
            <?php endif; ?>
            <?php endif; ?>

        </section>
        <!-- Section: Premium -->

    </div>

    
</main>
<div class="container my-5 z-depth-1 pt-5" style="height: 80vh; margin-top: 130px!important;">

  <!--Section: Content-->
  <section class="dark-grey-text mt-5 pt-5">

    <div class="row pr-lg-5">
      <div class="col-md-7 mb-4">

        <div class="view">
          <img src="<?php echo base_url(); ?>assets/front/img/payment.jpg" class="img-fluid">
        </div>

      </div>
      <div class="col-md-5 d-flex align-items-center">
        <div>
          
          <h3 class="font-weight-bold mb-4">Dziękujemy za zakupy w naszym sklepie</h3>
          <h4 class="font-weight-bold mb-4">Twoje zamówione książki znajdą się w twojej wirtualnej bibliotece</h4>

        </div>
      </div>
    </div>

  </section>
  <!--Section: Content-->


</div>
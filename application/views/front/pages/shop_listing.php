
        <div class="row mt-5">
        <?php $i=0; foreach ($books as $value):?>
            
            <div class="col-lg-3 mb-4">
            <a href="<?php echo base_url(); ?>ksiazka/<?php echo $value->id; ?>/<?php echo slug($value->title); ?>">
                <!--Card-->
                <div class="card card-cascade card-ecommerce">

                    <!--Card image-->
                    <div class="view view-cascade overlay bg-photo-card" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>">

                            <div class="mask rgba-white-slight"></div>

                    </div>
                    <!--/.Card image-->

                    <!--Card content-->
                    <div class="card-body card-body-cascade text-center">

                        <h5><span class="text-dark font-italic"><?php echo $value->author ?></span></h5>
                        <h4 class="card-title"><strong><?php echo $value->title ?></strong></h4>

                        <!--Description-->
                        <div class="card-text text-dark"><?php echo substr($value->description, 0, 50) ?>...
                        </div>


                        <!--Card footer-->
                        <div class="card-footer">

                            <span class="float-left text-dark"><?php echo $value->price ?> zł </span>

                            <?php if(isset($_SESSION['login_client']) && $_SESSION['login_client'] == true): ?>
                            
                            <?php $bookInCart = false;
                            foreach ($this->cart->contents() as $cart) {
                                if($cart['book_id'] == $value->id) {
                                    $bookInCart = true;
                                }
                            } ?>
                            <?php if($bookInCart == false): ?>
                                <a class="icons-sm so-ic float-right" href="<?= base_url('cart/add_good/'.$value->id); ?>">
                                    <i class="fas fa-cart-plus text-dark"></i>
                                </a>
                            
                            <?php else: ?>
                            
                                <a class="icons-sm so-ic float-right">
                                    <i class="fas fa-check"></i>
                                </a>
                            <?php endif; ?>
                            <?php endif; ?>

                        </div>

                    </div>
                </div>
            </a>
            </div>


        <?php $i++; endforeach; ?>

        </div>

<main>

    <div class="container pt-5 mt-5">

        <section class="text-center team-section wow fadeIn" data-wow-delay="0.3s">

            <!--Grid row-->
            <div class="row text-center">

                <!--Grid column-->
                <div class="col-md-12 mt-5 mb-5">

                    <!--Section: edit profile-->
                    <section class="wow fadeIn" data-wow-delay="0.5s">

                        <h2 class="h1-responsive font-weight-bold text-center ">Edycja profilu</h2>
                        <hr class=" hr-dark ">


                        <!--Tab panels -->
                        <div class="tab-content">

                            <!--personal data-->
                            <div class="tab-pane fade show active" id="PersonalData" role="tabpanel">
                                <br>
                                <form action="<?php echo base_url(); ?>klienci/update_client" method="post" class="modal-body mb-1" enctype="multipart/form-data">
                                <div class="row text-center">
                                <div class="col-md-6 mt-5">
                                    <div class="file-field">
                                                <div id="photoViewer_1" class="mb-4 delete_photo cursor" onclick="clear_box(1);">
                                                <?php if($user_data->photo != '') {
                                                    echo '<img class="rounded-circle z-depth-1-half avatar-pic" src="' . base_url() . 'uploads/' . $user_data->photo . '" width=200px>';
                                                } else {
                                                    echo '<img class="rounded-circle z-depth-1-half avatar-pic" src="http://via.placeholder.com/64x64" alt="">';
                                                } ?>
                                                </div>
                                                
                                                <div class="md-form">
                                                <div class="file-field d-flex justify-content-center">
                                                    <div class="btn btn-dark btn-md">
                                                    <span>Dodaj avatar</span>
                                                    <input type="file" name="photo_1" id="photo_1">
                                                    </div>
                                                    <div class="file-path-wrapper">
                                                    <input name="name_photo_1" id="name_photo_1" class="file-path validate" type="text" placeholder="Dodaj avatar" required readonly>
                                                    </div>
                                                </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-5">
                                        <div class="md-form">
                                        <input type="text" id="name" class="form-control mb-4"
                                               placeholder="imie" value="<?php echo @$user_data->first_name; ?>" name="first_name" required>
                                        </div>
                                        <div class="md-form">
                                        <input type="text" id="surname" class="form-control mb-4"
                                               placeholder="nazwisko" value="<?php echo @$user_data->last_name; ?>" name="last_name" required>
                                        </div>
                                        <div class="md-form">
                                        <input type="email" id="email" class="form-control mb-4 validate"
                                               placeholder="email" value="<?php echo @$user_data->email; ?>" readonly required>
                                        </div>
                                        <!--Textarea with icon prefix-->
                                        <div class="md-form">
                                            <textarea id="form10" class="md-textarea form-control" rows="3" name="description" required><?php echo @$user_data->description; ?></textarea>
                                            <label for="form10">opis</label>
                                        </div>
                                        <button id="SaveChangesPersonalData"
                                                class="btn btn-outline-elegant btn-md waves-effect pr-5 pl-5 btn-rounded float-right">
                                            Zapisz zmiany
                                        </button>
                                    </div>
                                </div>
                            </form>
                            </div>
                            <!--/personal data-->


                        </div>


                    </section>
                    <!--/Section: edit profile-->

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </section>
        <!--/Section: about me-->


    </div>

</main>

    <div class="container pt-5 ">
    <section class="section extra-margins text-center text-lg-left wow fadeIn" data-wow-delay="0.3s">
    <h2 class="font-weight-bold text-center h1 my-5 pt-5 pb-3 ">Książki w naszej ofercie</h2>
    
            <div class="books__search">
        <div class="books__container container">

          <select class="books__select browser-default custom-select col-md-3" name="kategoria" id="kategoria" onchange="search_books_categories();">
            <option disabled selected value="none">Kategoria</option>
                  <option value="none">brak filtra</option>
                  <?php $i=0; foreach ($categories as $row):?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->category_name; ?></option>
                  <?php $i++; endforeach; ?>
          </select>

        </div>
      </div>       
    <div class="container-fluid">
        <div class="books_list" id="list">

        </div>
      </div>
</div>
</section>
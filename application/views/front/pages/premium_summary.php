<div class="container my-5 z-depth-1 pt-5" style="height: 80vh; margin-top: 130px!important;">

  <!--Section: Content-->
  <section class="dark-grey-text mt-5 pt-5">

    <div class="row pr-lg-5">
      <div class="col-md-7 mb-4">

        <div class="view">
          <img src="<?php echo base_url(); ?>assets/front/img/payment.jpg" class="img-fluid">
        </div>

      </div>
      <div class="col-md-5 d-flex align-items-center">
        <div>
          
          <h3 class="font-weight-bold mb-4">Dziękujemy za zakup konta premium</h3>
          <h4 class="font-weight-bold mb-4">Wyloguj się oraz zaloguj ponownie w celu uzyskania benefitów z  pakietu premium</h4>
          <h4 class="font-weight-bold mb-4">W ramach pakietu premium uzyskałeś:</h4>

             <div><?php echo $premium_user->description ?></div>


        </div>
      </div>
    </div>

  </section>
  <!--Section: Content-->


</div>
<!--Main Layout-->
<?php $user = $this->back_m->get_user_data($user_ebook->user_id); ?>
<main>

    <div class="container pt-5 mt-5">


        <!--Grid row-->
        <div class="row ">

            <!--Grid column-->
            <div class="col-md-6 mt-5 mb-5">
                <img src="<?php echo images() . '/' . $user_ebook->photo; ?>" class="img-fluid z-depth-1"
                     alt="image">
            </div>
            <!--Grid column-->
            <div class="col-md-6 mt-5 mb-5 text-center text-md-left">
                <h2 class="font-weight-bold dark-grey-text "><?php echo $user_ebook->book_title ?></h2>
                <hr class="mb-5">
                <h5 class="font-weight-light mb-3">
                    Autor: <a href="<?php echo base_url(); ?>profil_uzytkownika/<?php echo $user->id; ?>"><?php echo $user->email ?> </a></h5>
                    
                <a href="<?php echo base_url(); ?>czytaj/<?php echo $user_ebook->id; ?>/<?php echo slug($user_ebook->book_title); ?>"
                   class="btn btn-primary">Czytaj</a>
                <?php if (@$_SESSION['premium'] == '1'): ?>
                    <a href="<?php echo base_url(); ?>pobierz_pdf/<?php echo $user_ebook->id; ?>/<?php echo slug($user_ebook->book_title); ?>"
                       class="btn btn-primary" target="_blank">Pobierz PDF</a>
                <?php endif; ?>

            </div>
            <div class="col-md-12 mb-5">
                <h2 class="font-weight-bold dark-grey-text">Opis książki: </h2>
                <hr>
                <p> <?php echo $user_ebook->description ?> </p>
            </div>
        </div>
        <!--Grid row-->


    </div>

    <div class="container">

        <div class="row justify-content-center ">
            <!-- Grid column -->
            <div class="col-md-12 mt-4">
                <!--Comments-->
                <div class="card card-cascade wider card-comments mb-4 wow fadeIn lighten-4">
                    <div class="view view-cascade gradient-card-header blue-gradient font-weight-bold white h3 ml-3 mr-3 text-center">Komentarze</div>
                    <div class="card-body card-body-cascade p-4" id="comments">
                        <?php $data = $this->news_m->get_comments('write_ebook', $this->uri->segment(3)); ?>
                        <?php if ($data) { ?>
                            <?php foreach ($data as $row): ?>
                                <div class="media d-block d-md-flex mt-3 white p-3 z-depth-1">
                                    <div class="media-body text-center text-md-left ml-md-3 ">
                                        <h5 class="mt-0 font-weight-bold"><?php echo $row->username; ?> <span
                                                    class="font-weight-light text-center float-md-right"><i class="far fa-clock mr-2"></i><small><?php echo $row->date; ?> </small>
                              </span></h5>
                                        <hr class="mb-4">
                                        <?php echo $row->content; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php } else { ?>
                            <div class="media d-block d-md-flex mt-3">
                                <div class="media-body text-center text-md-left ml-md-3 ml-0">
                                    <h5 class="mt-0 font-weight-bold">Brak komentarzy</h5>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <!--/.Comments-->

                <!--Reply-->
                <?php if (isset($_SESSION['login_client']) && $_SESSION['login_client'] == true) { ?>
                    <div class="card card-cascade wider mb-3 wow fadeIn pr-5 pl-5">
                        <div class="card-header view view-cascade font-weight-bold text-center white ">Napisz komenatarz</div>
                        <div class="pr-4 pl-4">

                            <!-- Default form reply -->
                            <form>
                                <div id="success">

                                </div>
                                <!-- Comment -->
                                <div class="form-group">
                                    <textarea class="form-control" id="replyFormComment" rows="5" required></textarea>
                                </div>


                                <div class="text-right mt-4">
                                    <button type="button" class="btn primary-color text-white"
                                            onclick="add_comment('write_ebook');">Dodaj komentarz
                                    </button>
                                </div>
                            </form>
                            <!-- Default form reply -->


                        </div>
                    </div>
                <?php } else { ?>
                    <div class="card mb-3 wow fadeIn">
                        <div class="card-header font-weight-bold">Zaloguj się aby móc dodawać komentarze!</div>

                    </div>
                <?php } ?>
                <!--/.Reply-->
            </div>
        </div>
    </div>

    <!-- /.Section: Blog -->
</main>
<?php $this->load->view('front/blocks/comment.php') ?>
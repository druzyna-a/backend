<?php $user = $this->back_m->get_user_data($entry->user_id); ?>
<main>
    <!-- Section: Blog  -->
    <div class="container">

        <!-- Grid row -->
        <div class="row justify-content-center mt-5 pt-5 mb-5 pb-5 wow fadeIn" data-wow-delay="0.3s">

            <!-- Grid column -->
            <div class="col-md-6 ">

                <!-- Card -->
                <div class="card card-cascade wider reverse md pt-5 ">

                    <!-- Card image -->
                    <div class="view view-cascade overlay ">
                        <img class="card-img-top w-100" src="<?php echo images().'/'.$entry->photo ?>" alt="Sample image">

                    </div>

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center ">

                        <!-- Title -->
                        <h2 class="font-weight-bold "><a class="dark-grey-text" href=""><?php echo $entry->title ?></a></h2>
                        <!-- Data -->

                    </div>
                </div>
                </div>
                <!-- Card -->

                <div class="col-md-6 mt-2">
                <?php if($entry->user_id != '0'): ?>
                <div class="text-center pt-5">
                <?php if(@$user->photo != '') {
                    echo '<img class="rounded-circle z-depth-1 card-img-100 img-fluid" src="' . base_url() . 'uploads/' . @$user->photo . '" width=200px>';
                    } else {
                            echo '<img class="rounded-circle z-depth-1 card-img-100 img-fluid" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg" alt="ebookapp">';
                    } ?>
                </div>
                <div class="text-center pt-3">
                    <h5 class="font-weight-bold ">
                        <a class="text-primary" href="<?php echo base_url(); ?>profil_uzytkownika/<?php echo $user->id; ?>"><?php echo $user->email ?></a>
                        <br>
                        <a class="text-light" href="<?php echo base_url(); ?>profil_uzytkownika/<?php echo $user->id; ?>"><?php echo $entry->created ?></a>
                    </h5>
                </div>
                <?php else: ?>
                    <div class="text-center pt-5">
                    <img class="rounded-circle z-depth-1 card-img-100 img-fluid"
                         src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg"
                         alt="ebook app">
                </div>
                <div class="text-center pt-3">
                    <h5 class="font-weight-bold ">
                        <span class="text-primary">administracja</span>
                        <br>
                        <span class="text-light"><?php echo $entry->created ?></span>
                    </h5>
                </div>
                <?php endif; ?>

                <!-- Excerpt -->
                <div class="mt-2 pb-3 ">

                    <p>
                    <?php echo $entry->description ?>
                    </p>

                </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->
        <hr class="mt-5  hr-dark ">
        <div class="row justify-content-center ">
            <!-- Grid column -->
      <div class="col-md-12">
        <!--Comments-->
        <div class="card card-cascade wider card-comments mb-4 wow fadeIn lighten-4">
            <div class="view view-cascade gradient-card-header blue-gradient font-weight-bold white h3 ml-3 mr-3 text-center">Komentarze</div>
            <div class="card-body card-body-cascade p-4" id="comments">
            	<?php $data = $this->news_m->get_comments('blog',$this->uri->segment(3)); ?>
            		<?php if($data){ ?>
                  <?php foreach ($data as $row):?>
                      <div class="media d-block d-md-flex mt-3 white p-3 z-depth-1">
                          <div class="media-body text-center text-md-left ml-md-3 ">
                              <h5 class="mt-0 font-weight-bold"><?php echo $row->username; ?> <span
                                          class="font-weight-light text-center float-md-right"><i class="far fa-clock mr-2"></i><small><?php echo $row->date; ?> </small>
                              </span></h5>
                              <hr class="mb-4">
                              <?php echo $row->content; ?> 
                          </div>
                      </div>
                  <?php endforeach; ?>
                <?php }else{ ?>
                      <div class="media d-block d-md-flex mt-3">
                          <div class="media-body text-center text-md-left ml-md-3 ml-0">
                              <h5 class="mt-0 font-weight-bold">Brak komentarzy</h5>
                          </div>
                      </div>
                  <?php } ?>

            </div>
        </div>
        <!--/.Comments-->

        <!--Reply-->
        <?php if(isset($_SESSION['login_client']) && $_SESSION['login_client'] == true){ ?>
        <div class="card card-cascade wider mb-3 wow fadeIn pr-5 pl-5">
            <div class="card-header view view-cascade font-weight-bold text-center white">Napisz komentarz</div>
            <div class="pr-4 pl-4">

                <!-- Default form reply -->
                <form>
                    <div id="success">
                      
                    </div>
                    <!-- Comment -->
                    <div class="form-group">
                        <textarea class="form-control" id="replyFormComment" rows="5" required></textarea>
                    </div>


                    <div class="text-right mt-4">
                        <button type="button" class="btn primary-color text-white" onclick="add_comment('blog');">Dodaj komentarz</button>
                    </div>
                </form>
                <!-- Default form reply -->



            </div>
        </div>
    	<?php }else{ ?>
        <div class="card mb-3 wow fadeIn">
            <div class="card-header font-weight-bold">Zaloguj się aby móc dodawać komentarze!</div>

        </div>
        <?php } ?>
        <!--/.Reply-->
      </div>
        </div>
    </div>
    <!-- /.Section: Blog -->
</main>
<?php $this->load->view('front/blocks/comment.php') ?>
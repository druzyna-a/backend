<main>

    <div class="container p-5">

        <!--Section: Blog -->
        <section class="section extra-margins text-center text-lg-left wow fadeIn" data-wow-delay="0.3s">
            <!--Section heading-->
            <h2 class="font-weight-bold text-center h1 my-5 pt-5 pb-3 ">Najnowsze wpisy na blogu</h2>
            
            <?php $i=0; foreach (array_reverse($blog) as $key):?>
            <?php $user = $this->back_m->get_user_data($key->user_id); ?>

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-lg-4 mb-4">
                    <!--Featured image-->
                    <div class="overlay z-depth-1" style="width: 100%">
                        <img src="<?php echo base_url(); ?>uploads/<?php echo $key->photo; ?>" class="img-fluid" alt="<?php echo $key->alt; ?>">
                        <a>
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-7 mb-4">

                    <h4 class="mb-4"><strong> <?php echo $key->title ?> </strong></h4>
                    <p><?php echo substr($key->description, 0, 130) ?>...</p>
                    <?php if($key->user_id != '0'): ?>
                    <p><span class="font-weight-bold text-dark font-weight-bold" href=""><?php echo $user->email ?></span>, <?php echo $key->created ?></p>
                    <?php else: ?>
                    <p><span class="font-weight-bold text-dark font-weight-bold" href="">administracja</span>, <?php echo $key->created ?></p>
                    <?php endif; ?>
                    <a class="btn primary-color text-white" href="<?php echo base_url(); ?>blog_wpis/<?php echo $key->id; ?>/<?php echo slug($key->title); ?>">czytaj więcej</a>

                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
            <?php $i++; endforeach; ?>

        </section>
        <!--Section: Blog -->
    </div>

</main>

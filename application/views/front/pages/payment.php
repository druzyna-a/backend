<?php $suma = 0;
foreach ($this->cart->contents() as $value) {
    $suma += $value['price'];
} ?>

<?php
$book_id = '';
$name = '';
$price = '';
$amount = '';
foreach ($this->cart->contents() as $value) {
    $book_id .= $value['book_id'] . '&';
    $name .= $value['name'] . '&';
    $price .= $value['price'] . '&';
    $amount .= $value['qty'] . '&';
}
$book_id = rtrim($book_id, "&");
$name = rtrim($name, "&");
$price = rtrim($price, "&");
$amount = rtrim($amount, "&"); ?>

<main>

    <form method="post" action="<?= base_url('payment/platnosc'); ?>" class="container pt-5 mt-5 mb-5">
        <div class="row text-center ">
            <div class="card col-md-6 mr-2 ml-2">
                <div class="card-body">
                    <h2>Dane do zamówienia</h2>
                    <div class="form">
                        <div class="col">
                            <!-- First name -->
                            <div class="md-form">
                                <input type="text" id="first_name" name="first_name" class="form-control" required="" value="<?php echo $_SESSION['first_name']; ?>">
                                <label for="first_name">Imię</label>
                            </div>
                            <!-- Last name -->
                            <div class="md-form">
                                <input type="text" id="last_name" name="last_name" class="form-control" required="" value="<?php echo $_SESSION['last_name']; ?>">
                                <label for="last_name">Nazwisko</label>
                            </div>
                            <!-- email -->
                            <div class="md-form">
                                <input type="email" id="p24_email" name="p24_email" class="form-control" required="" value="<?php echo $_SESSION['email']; ?>">
                                <label for="p24_email">Adres e-mail</label>
                            </div>
                            <!-- phone -->
                            <div class="md-form">
                                <input type="tel" id="phone" name="phone" class="form-control" required="">
                                <label for="phone">Telefon</label>
                            </div>
                            <!-- address -->
                            <div class="md-form">
                                <input type="text" id="p24_address" name="p24_address" class="form-control" required="">
                                <label for="p24_address">Ulica, nr budynku, nr mieszkania</label>
                            </div>
                            <!-- zip code -->
                            <div class="md-form">
                                <input type="text" id="p24_zip" name="p24_zip" class="form-control" required="">
                                <label for="p24_zip">Kod pocztowy</label>
                            </div>
                            <!-- city -->
                            <div class="md-form">
                                <input type="text" id="p24_city" name="p24_city" class="form-control" required="">
                                <label for="p24_city"> Miasto </label>
                            </div>


                            <input type="hidden" name="env" value="1"/>
                            <input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>"/>
                            <input type="hidden" name="book_id" value="<?php echo $book_id; ?>"/>
                            <input type="hidden" name="name" value="<?php echo $name; ?>"/>
                            <input type="hidden" name="qty" value="<?php echo $amount; ?>"/>
                            <input type="hidden" name="price" value="<?php echo $price; ?>"/>
                            <input type="hidden" name="redirect" value="off"/>
                            <input type="hidden" name="salt" value="dc20fa75df8d435c"/>
                            <input type="hidden" name="p24_merchant_id" value="24816"/>
                            <input type="hidden" name="p24_pos_id" value="24816"/>
                            <input type="hidden" name="p24_sign"
                                   value="<?php echo md5(session_id() . '|24816|' . ($suma * 100) . '|PLN|dc20fa75df8d435c'); ?>"/>
                            <input type="hidden" name="p24_session_id" value="<?php echo session_id(); ?>"/>
                            <input type="hidden" name="p24_amount" value="<?php echo floor($suma * 100); ?>"/>
                            <input type="hidden" name="p24_currency" value="PLN"/>
                            <input type="hidden" name="p24_url_status"
                                   value="<?php echo base_url(); ?>payment/status?ok=1"/>
                            <input type="hidden" name="p24_url_return"
                                   value="<?php echo base_url(); ?>payment/podsumowanie?ok=1"/>
                            <input type="hidden" name="p24_api_version" value="3.2"/>
                            <input type="hidden" name="p24_quantity_1" value="<?php echo $value['qty']; ?>"/>
                            <input type="hidden" style="width:250px" name="p24_wait_for_result" value="0"/>


                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5 ml-3 mt-5 text-left">
                <h4 class="payment_title">Wybierz metodę płatności:</h4>

                <div class="payment__radio-box mb-0">
                    <!-- Default unchecked -->
                    <div class="payment__radio-input custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="payment1" name="payment"
                               value="0">
                        <label class="custom-control-label" for="payment1">Przelewy 24</label>
                    </div>

                </div>
                <div class=" mt-5" >

                    <h4>Kwota zamówienia: <strong> <?= str_replace(',', ' ', number_format($suma)); ?> zł</strong> </h4>
                    <button type="button" onclick="window.history.back();" class=" mt-3 btn btn-primary btn-rounded ">wróć
                    </button>
                    <button type="submit" class=" mt-3 btn btn-primary btn-rounded">zapłać
                        <i class="fas fa-angle-right right"></i>
                    </button>
                </div>
            </div>


        </div>
    </form>

</main>
<!--Main Layout-->
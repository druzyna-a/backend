<div class="container pt-5 mt-5">


  <!-- Section: Block Content -->
  <section class="section extra-margins dark-grey-text text-center">

  <h2 class="font-weight-bold text-center h1 my-5 pt-5 pb-3 ">Książki naszych użytkowników</h2>

    <!-- Grid row -->
    <div class="row">

      <?php $i=0; foreach (array_reverse($user_ebooks) as $value):?>
      <!-- Grid column -->
      <div class="col-lg-4 col-md-12 mb-4">

        <div class="view overlay rounded z-depth-1">
          <img src="<?php echo images().'/'.$value->photo; ?>" class="img-fluid" alt="Sample project image" style="width: 100%">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>

        <div class="px-3 pt-3 mx-1 mt-1 pb-0">
          <h4 class="font-weight-bold mt-1 mb-3"><?php echo $value->book_title ?></h4>
          <p class="text-muted"><?php echo substr($value->description, 0, 50) ?>...</p>
          <a href="<?php echo base_url(); ?>ksiazka_uzytkownika/<?php echo $value->id; ?>/<?php echo slug($value->book_title); ?>" class="btn btn-primary"><i class="fas fa-clone left"></i> Czytaj więcej</a>
        </div>

      </div>
      <!-- Grid column -->
      <?php $i++; endforeach; ?>
      

    </div>
    <!-- Grid row -->

  </section>
  <!-- Section: Block Content -->


</div>
<!--Main Layout-->
<main>

    <div class="container pt-5 mt-5">


        <!--Grid row-->
        <div class="row ">

            <!--Grid column-->
            <div class="col-md-6 mt-5 mb-5">
                <img src="<?php echo images().'/'.$book->photo; ?>" class="img-fluid z-depth-1"
                     alt="image">
            </div>
            <!--Grid column-->
            <div class="col-md-6 mt-5 mb-5 text-center text-md-left">
                <h2 class="font-weight-bold dark-grey-text "><?php echo $book->title ?></h2>
                <hr class="mb-5">
                <h5 class="font-weight-light mb-3">Autor: <?php echo $book->author ?> </h5>
                <h5 class="font-weight-light mb-3">Cena: <?php echo $book->price ?> zł </h5>
                <h5 class="font-weight-light mb-3">Wydawnictwo: <?php echo $book->publishing_house ?> </h5>
                <h5 class="font-weight-light mb-3">Rok wydania: <?php echo $book->year ?> </h5>
                <h5 class="font-weight-light mb-3">Tagi: <?php echo $book->tags ?> </h5>


                <?php if(isset($_SESSION['login_client']) && $_SESSION['login_client'] == true): ?>
                <?php $bookInCart = false;
                foreach ($this->cart->contents() as $cart) {
                    if($cart['book_id'] == $book->id) {
                        $bookInCart = true;
                    }
                } ?>
                <?php if($bookInCart == false): ?>
                    <a href="<?= base_url('cart/add_good/'.$book->id); ?>" class="btn btn-primary">Kup teraz</a>
                <?php else: ?>
                    <button type="button" class="btn btn-primary" disabled>Produkt już w koszyku</button>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-md-12">
                <h2 class="font-weight-bold dark-grey-text">Opis książki: </h2>
                <hr>
                <p> <?php echo $book->description ?> </p>
            </div>
        </div>
        <!--Grid row-->


    </div>

</main>
<!--Main Layout-->
<div class="container mt-5 pt-5">
    <div class="card card-cascade wider reverse">
        <div class="card-body card-body-cascade text-center mt-md-3 mb-3">
            <?php $i = 0;
            foreach ($read_ebook as $value): ?>
                <p class="h1 mt-5"><?php echo $value->chapter_title ?></p>
                <hr width="50" class=" mb-5">
                <div>

                    <?php echo $value->chapter_content ?>

                </div>
            <?php break;  endforeach; ?>

        </div>
    </div>
</div>
<section>
    <div class="container pt-5 mt-5">

        <div class="row text-center">

            <div class="col-md-12 mt-5 mb-5">

                <h2 class="h1-responsive font-weight-bold text-center ">Twoja biblioteka</h2>
                <hr class=" hr-dark ">


            </div>

        </div>

        <!-- Table with panel -->
        <div class="card card-cascade narrower mb-5">

            <!--Card image-->
            <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <div class="md-form my-0">
                    <a href="" class="white-text mx-3">Twoje zakupione książki</a>
                </div>

                <div class="text-right" style="width: 181px;">

                </div>

            </div>
            <!--/Card image-->

            <div class="px-4 pb-3">

                <div class="table-wrapper">
				    <span id="refreshTable">
				      <!--Table-->
				      <table id="filtrableTable" class="table table-hover table-responsive mb-0">

				        <!--Table head-->
				        <thead>
				          <tr>
                              <th></th>
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 30%;">
				              Tytuł
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableData(1)" style="width: 20%;">
				              Data zakupu
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th style="width: 20%;"></th>
				          </tr>
				        </thead>
                          <!--Table head-->

                          <!--Table body-->
				        <tbody>
							<?php foreach (array_reverse($user_transactions) as $transactions): ?>

                        <?php $X = explode("&", $transactions->book_id); ?>

                        <?php foreach (array_reverse($X) as $value): ?>
                        <?php $data['book'] = $this->back_m->get_one('books', $value); ?>
                        <tr id="<?php echo $data['book']->id ?>">
						            <td class="align-middle">
						            	<img src="<?php echo base_url(); ?>uploads/<?php echo $data['book']->photo; ?>"
                                             class="img-fluid">
						            </td>
                        <td class="align-middle"><?php echo $data['book']->title; ?></td>
						            <td class="align-middle"><?php echo $transactions->created; ?></td>
						            <td class="align-middle text-right">
									<a href="<?php echo base_url(); ?>uploads/<?php echo $data['book']->file; ?>"
                                       title="Pobierz">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									      <i class="fas fa-download mt-0"></i>
									    </button>
									  </a>
								  	</td>
				            	</tr>

                                <?php endforeach; ?>

                            <?php endforeach; ?>

				        </tbody>
                          <!--Table body-->
				      </table>
                        <!--Table-->
			      </span>
                </div>

            </div>

        </div>
        <!-- Table with panel -->
    </div>
</section>
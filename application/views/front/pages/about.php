<section class="container-fluid">
    <h2 class="h1-responsive font-weight-bold text-center mb-5 mt-5" style="margin-top: 146px!important">O nas</h2>
    <div class="row">
    <?php $i=0; foreach ($about as $value): $i++; if($i%2!=0): ?>
        <div class="col-12 bg-photo d-lg-none" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>
        <div class="col-lg-6 bg-muted first_section">
            <h1 class="mb-4 wow fadeInLeft"><?php echo $value->title; ?></h1>
                <div class="font-regular text-muted mb-0 wow fadeInLeft"><?php echo $value->description; ?></div>
            </div>
            <div class="col-lg-6 bg-photo d-none d-lg-block" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>

            <?php else: ?>

            <div class="col-lg-6 bg-photo d-none d-lg-block" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>
            <div class="col-12 bg-photo d-lg-none" style="background-image: url('<?php echo images().'/'.$value->photo; ?>');" title="<?php echo $value->alt; ?>"></div>
        <div class="col-lg-6 first_section">
            <h1 class="mb-4 wow fadeInRight"><?php echo $value->title; ?></h1>
            <div class="font-regular text-muted mb-0 wow fadeInRight"><?php echo $value->description; ?></div>
        </div>
    <?php endif; endforeach ?>
    </div>
</section>
<div class="container mt-5">    
        <!-- premium  -->
        <!-- Section: Premium -->
        <section class="text-center my-5">
            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold text-center mb-5 mt-5">Zostań użytkownikiem premium</h2>
            <!-- Section description -->

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-lg-6 col-md-12 mb-lg-0 mb-4">

                    <!-- Card -->
                    <div class="card">

                        <!-- Content -->
                        <div class="card-body main_page_card">

                            <!-- Offer -->
                            <h4 class="mb-4 font-weight-bold"><?php echo $normal_user->title ?></h4>
                            <hr>
                            <div class=" justify-content-center">
                            <?php echo $normal_user->description ?>
                            </div>
                            <hr>
                            <!-- Price -->
                            <h2 class="font-weight-bold my-4"><?php echo $normal_user->price ?></h2>


                        </div>
                        <!-- Content -->

                    </div>
                    <!-- Card -->

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-6 col-md-6 mb-md-0 mb-4">

                    <!-- Card -->
                    <div class="card blue-gradient">

                        <!-- Content -->
                        <div class="card-body white-text main_page_card">

                            <!-- Offer -->
                            <h4 class="mb-4 font-weight-bold"><?php echo $premium_user->title ?></h4>
                            <hr>
                            <div class=" justify-content-center">
                            <?php echo $premium_user->description ?>
                            </div>
                            <hr>
                            <!--Price -->
                            <h2 class="font-weight-bold my-3"><?php echo $premium_user->price ?></h2>


                        </div>
                        <!-- Content -->

                    </div>
                    <!-- Card -->

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </section>
        <!-- Section: Premium -->

    </div>
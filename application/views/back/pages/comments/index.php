    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="pd-30">
        <h4 class="tx-gray-800 mg-b-5"><?php echo ucfirst(str_replace('_', ' ', $this->uri->segment(2))); ?></h4>
        <p class="mg-b-0"><?php echo subtitle(); ?></p>
        <hr>
      </div><!-- d-flex -->
      <div class="br-pagebody mg-t-0 pd-x-30">
        <?php if(isset($_SESSION['flashdata'])): ?>
        <div id="alert-box"><?php echo $_SESSION['flashdata']; ?></div>
        <?php endif; ?>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p align-top">L.p.</th>
                  <th class="wd-15p align-top">Typ wpisu</th>
                  <th class="wd-15p align-top">Wpis</th>
                  <th class="wd-35p align-top">Treść komentarza</th>
                  <th class="wd-30p text-right no-sort">
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; foreach (array_reverse($rows) as $value): $i++; ?>
                <?php $data['entry'] = $this->back_m->get_one($value->tablename, $value->item_id); ?>
                <tr>
                  <td class="align-middle"><?php echo $i; ?>.</td>
                  <td class="align-middle"><?php if($value->tablename == 'blog') echo 'Blog';
                   else echo 'Książka'; ?>
                  </td>
                  <td class="align-middle"><?php if($value->tablename == 'blog') echo $data['entry']->title;
                   else echo $data['entry']->book_title; ?>
                  </td>
                  <td class="align-middle"><?php echo substr($value->content, 0, 130) ?>...</td>
                  <td class="text-right">
                      <a href="<?php echo base_url(); ?>panel/settings/delete_comment/<?php echo $this->uri->segment(2); ?>/<?php echo $value->comments_id; ?>" class="btn btn-sm btn-secondary" 
                      onclick="return confirm('Czy na pewno chcesz usunąć <?php echo $value->content; ?>? #<?php echo $value->comments_id; ?>')" >
                        <i class="fa fa-close mg-r-10"></i> Usuń
                      </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div><!-- table-wrapper -->
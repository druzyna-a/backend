    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="pd-30">
        <h4 class="tx-gray-800 mg-b-5"><?php echo ucfirst(str_replace('_', ' ', $this->uri->segment(2))); ?></h4>
        <p class="mg-b-0"><?php echo subtitle(); ?></p>
        <hr>
      </div><!-- d-flex -->
      <div class="br-pagebody mg-t-0 pd-x-30">
        <?php if(isset($_SESSION['flashdata'])): ?>
        <div id="alert-box"><?php echo $_SESSION['flashdata']; ?></div>
        <?php endif; ?>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p align-top">L.p.</th>
                  <th class="wd-5p align-top">ID Usera</th>
                  <th class="wd-5p align-top">Imię i nazwisko</th>
                  <th class="wd-10p align-top">Email</th>
                  <th class="wd-20p align-top">Książki</th>
                  <th class="wd-20p align-top">ID Książek</th>
                  <th class="wd-10p align-top">Cena</th>
                  <th class="wd-25p text-right no-sort"></th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; foreach (array_reverse($rows) as $value): $i++; ?>
                <tr>
                  <td class="align-middle"><?php echo $i; ?>.</td>
                  <td class="align-middle"><?php echo $value->user_id; ?></td>
                  <td class="align-middle"><?php echo $value->first_name; ?> <?php echo $value->last_name; ?></td>
                  <td class="align-middle"><?php echo $value->p24_email; ?></td>
                  <td class="align-middle"><?php echo $value->name; ?></td>
                  <td class="align-middle"><?php echo $value->book_id; ?></td>
                  <td class="align-middle"> <?php echo number_format(($value->p24_amount/100),2); ?> zł</td>
                  <td class="text-right">
                      <a href="<?php echo base_url(); ?>panel/settings/delete/<?php echo $this->uri->segment(2); ?>/<?php echo $value->id; ?>" class="btn btn-sm btn-secondary" 
                      onclick="return confirm('Czy na pewno chcesz usunąć <?php echo $value->name; ?>? #<?php echo $value->id; ?>')" >
                        <i class="fa fa-close mg-r-10"></i> Usuń
                      </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div><!-- table-wrapper -->
<?php
    $status[0] = 'w trakcje realizacji';
    $status[1] = 'opłacone';
    $status[2] = 'anulowane przez administratora';
    $status[3] = 'anulowane przez klienta';
    $status[4] = 'zatwierdzone';
    $status[5] = 'oczekuje na kuriera';
    $status[6] = 'wysłane';
    ?>
    
<?php 
$pieces = explode("&", $row->qty); 
$i=0; 
foreach ($pieces as $value) { 
    $i++;
    $qty[$i] = $value;
} 
$pieces = explode("&", $row->price); 
$i=0; 
foreach ($pieces as $value) { 
    $i++;
    $price[$i] = $value;
} 
$pieces = explode("&", $row->comment); 
$i=0; 
foreach ($pieces as $value) { 
    $i++;
    $comment[$i] = $value;
} 
?>
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="pd-30">
        <h4 class="tx-gray-800 mg-b-5">Zamówienia</h4>
        <p class="mg-b-0"><?php echo subtitle(); ?></p>
        <hr>
      </div><!-- d-flex -->
      <div class="br-pagebody mg-t-0 pd-x-30">
        <?php if(isset($_SESSION['flashdata'])): ?>
        <div id="alert-box"><?php echo $_SESSION['flashdata']; ?></div>
        <?php endif; ?>

        <div class="card bd-0">
            <div class="card-header bg-dark text-white">
                Zamówienie nr #<?php echo $row->id; ?> z dnia <?php echo $row->created; ?>
            </div>
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="row">
                    <div class="col-md-4">
                        Status:
                        <span id="refreshSend">
                        <p class="font-weight-bold"><?php echo $status[$row->status]; ?></p>
                        </span>
                        Zmień na:
                        <ul class="pl-0" style="list-style-type: none">
                            <li><a onclick="changeStatus(<?php echo $row->id; ?>, 0)" class="text-primary" style="cursor: pointer;">w trakcje realizacji</a></li>
                            <li><a onclick="changeStatus(<?php echo $row->id; ?>, 1)" class="text-primary" style="cursor: pointer;">opłacone</a></li>
                            <li><a onclick="changeStatus(<?php echo $row->id; ?>, 2)" class="text-primary" style="cursor: pointer;">anulowane przez administratora</a></li>
                            <li><a onclick="changeStatus(<?php echo $row->id; ?>, 3)" class="text-primary" style="cursor: pointer;">anulowane przez klienta</a></li>
                            <li><a onclick="changeStatus(<?php echo $row->id; ?>, 4)" class="text-primary" style="cursor: pointer;">zatwierdzone</a></li>
                            <!-- <li><a onclick="changeStatus(<?php echo $row->id; ?>, 5)" class="text-primary" style="cursor: pointer;">oczekuje na kuriera</a></li>
                            <li><a onclick="changeStatus(<?php echo $row->id; ?>, 6)" class="text-primary" style="cursor: pointer;">wysłane</a></li> -->
                        </ul>
                    </div>
                    <div class="col-md-4">
                        Forma płatności:
                        <p class="font-weight-bold"><?php echo ucfirst($row->delivery); ?></p>
                        <hr>
                        Metoda wysyłki:
                        <p class="font-weight-bold">
                          <?php if($row->cost_ship == 16){echo 'DPD'; } ?>
                          <?php if($row->cost_ship == 20){echo 'Za pobraniem'; } ?>
                          <?php if($row->cost_ship == 0){echo 'Odbiór osobisty'; } ?>
                        </p>
                        <span id="refreshSend2">
                        <?php if($row->status != 0 && $row->status != 2 && $row->status != 3 && $row->status != 5 && $row->status != 6): ?>
                        <div class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modaldemo<?php echo $row->id; ?>">
                          <i class="icon fas fa-paper-plane mg-r-10"></i> Generuj wysyłkę
                        </div>
                        <?php endif; ?>
                        
                        <?php 
                        if(($row->status == 5 || $row->status == 6)): 
                        $dpd_label = $this->back_m->get_dpd('dpd_label', $row->id)->pdf;
                        $dpd_protocol = $this->back_m->get_dpd('dpd_label', $row->id)->protocol;?>
                        <a href="<?php echo base_url(); ?>panel/dpd_label/label/<?php echo $row->id;?>?pdf=<?php echo $dpd_label; ?>" class="btn btn-sm btn-secondary">
                          <i class="icon fas fa-tag mg-r-10"></i> Drukuj etykiete  
                        </a><br><br>
                        <a href="<?php echo base_url(); ?>panel/dpd_label/label/<?php echo $row->id;?>?pdf=<?php echo $dpd_protocol; ?>" class="btn btn-sm btn-secondary">
                          <i class="icon fas fa-tag mg-r-10"></i> Drukuj protokół  
                        </a>
                        <?php endif; ?>
                        </span>
                    </div>
                    <div class="col-md-4">
                        Klient:
                        <p class="font-weight-bold"><?php echo $row->first_name . ' ' . $row->last_name; ?><br>
                        <?php echo $row->p24_email; ?><br>
                        <?php echo $row->phone; ?><br>
                        <?php echo $row->p24_city . ' ' . $row->p24_zip; ?><br>
                        <?php echo $row->p24_address; ?><br>
                        <?php echo $row->p24_country; ?></p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        Komentarz do zamówienia:
                        <p class="font-weight-bold"><?php echo $row->p24_description; ?></p>
                        <?php if($row->nazwa != ''): ?>
                        Nazwa / numer na koszulce:
                        <p class="font-weight-bold"><?php echo $row->nazwa; ?></p>
                        <?php endif; ?>
                        <?php if($row->logo != ''): ?>
                        Logo:<br>
                        <img class="img-fluid" src="<?php echo base_url(); ?>uploads/<?php echo str_replace('&0' , '' ,$row->logo); ?>" width="100">
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        Ilość produktów:
                        <span class="font-weight-bold">
                            <?php $pieces = explode("&", $row->qty); 
                            $sum = 0;
                            $i=0; foreach ($pieces as $value) { $i++;
                                $sum += $value;
                                $amount[$i] = $value;
                            } 
                            echo $sum; ?>
                        </span>
                        <hr>
                        Koszt wysyłki:
                        <p class="font-weight-bold"><?php echo number_format($row->cost_ship,2); ?> PLN</p>
                        <hr>
                        Suma zamówienia:
                        <p class="font-weight-bold"><?php echo number_format(($row->p24_amount/100),2); ?> PLN</p>
                    </div>
                    <div class="col-md-4">
                        <?php if($row->company != ''): ?>
                        <button class="btn btn-sm btn-secondary btn-block" data-toggle="modal" data-target="#modaldemo1">
                            <i class="icon fas fa-file-pdf mg-r-10"></i> Faktura
                        </button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="card bd-0 mt-4">
            <div class="card-header bg-dark text-white">
                Zamówione produkty
            </div>
            <div class="card-body bd bd-t-0 rounded-bottom">
                <table class="table table-bordered">
                  <thead class="thead-colored thead-light">
                      <th>Lp</th>
                      <th>Nazwa produktu</th>
                      <th>Ilość</th>
                      <th>Cena jedn. netto</th>
                      <th>Wartość netto</th>
                      <th>VAT [%]</th>
                      <th>Kwota podatku</th>
                      <th>Całościowa wartość brutto</th>
                  </thead>
                  <tbody>
                    <?php
                    $pieces = explode("&", $row->name); 
                    $i=0;
                    foreach ($pieces as $value) {
                        $i++;
                        echo '<tr>';
                        echo '<td>' . $i . '. </td>';
                        if(isset($comment[$i]) && $comment[$i] != ''){
                          echo '<td data-container="body" data-toggle="popover" data-popover-color="default" data-placement="top" title="Komentarz do produktu" data-content="'.$comment[$i].'">' . $value . '</td>';
                        } else {
                          echo '<td>' . $value . '</td>';
                        }
                        echo '<td>' . $qty[$i] . '</td>';
                        echo '<td>' . number_format(($price[$i]*0.77),2) . ' PLN </td>';
                        echo '<td>' . number_format((($price[$i]*$qty[$i])*0.77),2) . ' PLN </td>';
                        echo '<td>23%</td>';
                        echo '<td>' . number_format(($price[$i] - ($price[$i]*0.77)),2) . ' PLN </td>';
                        echo '<td>' . number_format((($price[$i]*$qty[$i])),2) . ' PLN </td>';
                        echo '<tr>';
                    } ?>
                  </tbody>
                </table>
            </div>
        </div>

          <div id="modaldemo1" class="modal fade">
            <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Faktura nr <?php echo date('Y/m/d', strtotime($row->created)) . '/' . $row->id; ?></h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-25">
                  <div class="row">
                      <div class="col-12">
                          <h5>FAKTURA VAT<br>
                          <?php echo date('Y/m/d', strtotime($row->created)) . '/' . $row->id; ?></h5>
                      </div>
                    </div>
                    <div class="row mt-3">
                      <div class="col-6"></div>
                      <div class="col-6">
                        Miejsce i data wystawienia: <strong>Legnica, <?php echo date('Y-m-d', strtotime($row->created)); ?></strong><br>
                        Data sprzedaży: <strong><?php echo date('Y-m-d', strtotime($row->created)); ?></strong>
                      </div>
                    </div>
                    <div class="row mt-3">
                      <div class="col-6">
                          Sprzedawca:<br>
                          <strong><?php echo $contact->company; ?></strong><br>
                          <?php echo $contact->address; ?><br>
                          <?php echo $contact->zip_code . ' ' . $contact->city; ?><br>
                          NIP: 123 123 123
                      </div>
                      <div class="col-6">
                          Nabywca:<br>
                          <strong><?php echo $row->company; ?></strong><br>
                          <?php echo $row->address_company; ?><br>
                          <?php echo $row->zip_company . ' ' . $row->city_company; ?><br>
                          NIP:  <?php echo $row->nip; ?>
                      </div>
                  </div>
                    <div class="row mt-3">
                      <div class="col-12">
                          <table class="table table-bordered">
                              <thead class="thead-colored thead-light">
                                  <th>Lp</th>
                                  <th>Nazwa produktu</th>
                                  <th>Ilość</th>
                                  <th>Cena jedn. netto</th>
                                  <th>Wartość netto</th>
                                  <th>VAT [%]</th>
                                  <th>Kwota podatku</th>
                                  <th>Wartość brutto</th>
                              </thead>
                              <tbody>
                                <?php
                                $pieces = explode("&", $row->name); 
                                $i=0;
                                foreach ($pieces as $value) {
                                    $i++;
                                    echo '<tr>';
                                    echo '<td>' . $i . '. </td>';
                                    echo '<td>' . $value . '</td>';
                                    echo '<td>' . $qty[$i] . '</td>';
                                    echo '<td>' . ($price[$i]*0.77) . '</td>';
                                    echo '<td>' . (($price[$i]*$qty[$i])*0.77) . '</td>';
                                    echo '<td>23%</td>';
                                    echo '<td>' . ($price[$i]) . '</td>';
                                    echo '<td>' . (($price[$i]*$qty[$i])) . '</td>';
                                    echo '<tr>';
                                } ?>
                              </tbody>
                            </table>
                      </div>
                    </div>
                    <div class="row mt-3">
                      <div class="col-12">
                          <p class="mb-0">Koszty wysyłki: <?php echo number_format(($row->cost_ship),2); ?> zł</p><br>
                          <h5>Do zapłaty: <?php echo number_format(($row->p24_amount/100),2); ?> zł</h5><br>
                          Wystawił: <?php echo $contact->company; ?><br><br>
                          Numer konta: <strong><?php echo $settings->nr_konta; ?></strong><br>
                          Termin płatności upływa po 14 dniach: <?php echo date('Y-m-d', strtotime($row->created . '+ 14 days')); ?>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <a href="<?php echo base_url(); ?>panel/transakcje/pdf/<?php echo $row->id; ?>" type="button" class="btn btn-info tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Generuj PDF</a>
                  <button type="button" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

          <div id="modaldemo<?php echo $row->id; ?>" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
              <form action="<?php echo base_url(); ?>panel/<?php echo $this->uri->segment(2); ?>/send/<?php echo $row->id; ?>" method="post" class="modal-content bd-0 tx-14">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Notatka do zamówienia</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20">
                  <label>Waga paczki w KG</label>
                  <input class="form-control" type="number" name="waga" step="0.01">
                  <label class="mt-4">Opis dla kuriera</label>
                  <textarea rows="3" class="form-control" name="opis_kuriera"></textarea>
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="submit" class="btn btn-sm btn-secondary">Generuj wysyłkę</button>
                  <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Zamknij</button>
                </div>
              </form>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

    <script type="text/javascript">
      function changeStatus(id, status)
      {
        var id = id;
        var status = status;
        console.log(status);
        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/transakcje/status", 
             data: {id:id, status:status}, 
             cache: false,
             complete:function(html)
             {
                console.log(html);
                $('#refreshSend').load(document.URL +  ' #refreshSend');
                $('#refreshSend2').load(document.URL +  ' #refreshSend2');
             }  
        });  
      }
    </script>

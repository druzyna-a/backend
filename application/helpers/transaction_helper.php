<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    function add_transaction($post) {
        $CI = &get_instance();
        foreach ($post as $key => $value) {
			if (!$CI->db->field_exists($key, 'transaction')) {
				$CI->base_m->create_column('transaction', $key);
			}
			$insert[$key] = $value;
        }
        $CI->back_m->insert('transaction', $insert);
	}
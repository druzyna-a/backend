<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_ebook_front extends CI_Controller {

	public function index() {

            $data['contact'] = $this->back_m->get_one('contact_settings', 1);
            $data['settings'] = $this->back_m->get_one('settings', 1);
			$data['rows'] = $this->back_m->get_users_books('add_ebook', $_SESSION['id']);
			echo loadViewsFront('list_ebooks', $data);

	}

	public function form($type, $id = '') {

            if($id != '') {
			    $data['value'] = $this->back_m->get_one('add_ebook', $id);
            }
            $data['contact'] = $this->back_m->get_one('contact_settings', 1);
            $data['settings'] = $this->back_m->get_one('settings', 1);
			echo loadViewsFront('form_book', $data);

	} 

	public function action($type, $table, $id = '') {

			$this->form_validation->set_rules('book_title', 'Tytuł książki', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('description', 'Opis książki', 'min_length[2]|trim|required');

			$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
			$this->form_validation->set_message('required', 'Pole %s Jest wymagane');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('flashdata', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} else {

			$now = date('Y-m-d');
			if (!is_dir('uploads/'.$now)) {
				mkdir('./uploads/' . $now, 0777, TRUE);
			}
			$config['upload_path'] = './uploads/'.$now;
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = 0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			
			foreach ($_POST as $key => $value) {

				if (!$this->db->field_exists($key, $table)) {
					$this->base_m->create_column($table, $key);
				}

				if($key == 'name_photo_1') {
					if ($this->upload->do_upload('photo_1')) {
						$data = $this->upload->data();
						$insert['photo'] = $now.'/'.$data['file_name'];  
						addMedia($data);
					} elseif($value == 'usunięte') {
						$insert['photo'] = '';
					}
				} else {
					$insert[$key] = $value; 
				}
            }
            if($type == 'insert') {
                $insert['user_id'] = $_SESSION['id'];
				$this->back_m->insert($table, $insert);
				$this->back_m->log('dodano nową książkę'); 
			    $this->session->set_flashdata('flashdata', 'Rekord został dodany!');
            } else {
				$this->back_m->update($table, $insert, $id);
				$this->back_m->log('zaaktualizowano książkę o id '.$id.''); 
			    $this->session->set_flashdata('flashdata', 'Rekord został zaktualizowany!');   
            }

			redirect('add_ebook_front');

		}

    }
}
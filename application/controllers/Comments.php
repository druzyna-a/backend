<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends CI_Controller {


	public function add_comment($table, $id){

		$insert['content'] = $this->input->post('comment');
		$insert['tablename'] = $table;
		$insert['item_id'] = $id;
		$insert['user_id'] = $_SESSION['id'];
		$insert['username'] = $_SESSION['email'];
		$insert['date'] = $this->input->post('date');

		$this->back_m->log('dodano komentarz');
		$this->back_m->insert('comments', $insert);
	}
}
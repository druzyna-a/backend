<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klienci extends CI_Controller {

	public function login() {
		$this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim');
		$this->form_validation->set_rules('password', 'Hasło', 'min_length[6]|trim');
		$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');

		if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('flashdata', validation_errors());
			echo loadLogin('');
		} else {
			$login = strtolower($this->input->post('login'));
        	$password = $this->input->post('password');
			if($this->login_m->check_client($login, $password)){
				$this->back_m->log('udane logowanie');
				redirect('');
			} else {
				$this->back_m->log('nieudane logowanie');
				redirect('');
			}
		}
	}

	public function register() {
		$this->form_validation->set_rules('email', 'Adres e-mail', 'min_length[2]|trim|is_unique[klienci.email]|required');
		$this->form_validation->set_rules('password', 'Hasło', 'min_length[6]|trim|required');
		$this->form_validation->set_rules('confirm_password', 'Potwierdź hasło', 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
		$this->form_validation->set_message('required', 'Pole %s Jest wymagane');
		$this->form_validation->set_message('matches', 'Hasła różnią się od siebie');
		$this->form_validation->set_message('is_unique', 'Dany adres e-mail jest już w użyciu');

		if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('flashdata', validation_errors());
				redirect('');
		} else {
			$login = strtolower($this->input->post('login'));
        	$password = $this->input->post('password');
			$this->register_m->register($_POST);
			$this->session->set_flashdata('flashdata_success', 'Zostałeś pomyślnie zarejestrowany, proszę potwierdź swój adres e-mail klikająć w link na Twojej skrzynce pocztowej');
			$this->back_m->log('pomyślna rejestracja');
			redirect('');
			
		}
	}

	public function reset_pass() {
		$this->form_validation->set_rules('email', 'Adres E-mail', 'min_length[2]|trim|valid_email');
		$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
		$this->form_validation->set_message('valid_email', 'Błędny adres e-mail');

		if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('flashdata', validation_errors());
			redirect('');;
		} else {
			$email = strtolower($this->input->post('email'));
			if($this->login_m->check_email_client($email)){
				$this->back_m->log('pomyślny reset hasła');
				redirect('');
			} else {
				$this->back_m->log('nieudany reset hasła');
				redirect('');
			}
		}
	}

	public function logout() {
		$this->back_m->log('pomyślne wylogowanie');
		$this->session->sess_destroy();
		$this->session->set_flashdata('flashdata_success', 'Zostałeś pomyślnie wylogowany');
		redirect('');
	}
	
	public function active_account($special_key) {
		$insert['active'] = 1;
		$this->back_m->update_account('klienci', $insert, $special_key);
        $this->session->set_flashdata('flashdata_success', 'Twoje konto zostało aktywowane. Teraz możesz się zalogować.');
		redirect('');
	}

	public function update_client() {

			$this->form_validation->set_rules('first_name', 'Imię', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('last_name', 'Nazwisko', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('description', 'Opis', 'min_length[2]|trim|required');

			$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
			$this->form_validation->set_message('required', 'Pole %s Jest wymagane');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('flashdata', validation_errors());
				$this->back_m->log('nieudana edycja danych konta');
				redirect($_SERVER['HTTP_REFERER']);
			} else {

			$now = date('Y-m-d');
			if (!is_dir('uploads/'.$now)) {
				mkdir('./uploads/' . $now, 0777, TRUE);
			}
			$config['upload_path'] = './uploads/'.$now;
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = 0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			
			foreach ($_POST as $key => $value) {
				if($key == 'name_photo_1') {
					if ($this->upload->do_upload('photo_1')) {
						$data = $this->upload->data();
						$insert['photo'] = $now.'/'.$data['file_name'];   
						addMedia($data);
					} elseif($value == 'usunięte') {
						$insert['photo'] = '';
					}
				} elseif($key == 'password') {
					if($value != '') {
						$insert[$key] = hashPassword($value);
					}  
				} else {
					$insert[$key] = $value; 
				}
			}
		$this->back_m->update('klienci', $insert, $_SESSION['id']);
		$this->session->set_flashdata('flashdata_success', 'Dane zostały zapisane');  
		$this->back_m->log('pomyślna aktualizacja danych konta użytkownika'); 
		redirect('home/profile');
			}
    }
}
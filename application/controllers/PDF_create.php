<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf_create extends CI_Controller {

	public function create_pdf($id) {

        if(checkAccess($access_group = ['1'], $_SESSION['premium'])) {

        $data['user_ebook'] = $this->back_m->get_one('add_ebook', $id);

		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 0);
		require_once 'vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf();
        
        $mpdf->setFooter('{PAGENO}');

        $html=$this->load->view('front/pages/pdf_view/book_view', $data,true);  


		$mpdf->WriteHTML($html);
		ini_set('date.timezone', 'Europe/London');
        $now = date('Y-m-d-His');
        
        $mpdf->Output();
        $this->back_m->log('wygenerowano, oraz pobrano książke od id '.$id.''); 

    } else {
		redirect($_SERVER['HTTP_REFERER']);
	}
    }
    
}

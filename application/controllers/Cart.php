<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function add_good($id) {

		$book = $this->back_m->get_one('books', $id);
		$cart_list = array(
			'id'      => md5($book->id.'|'.$book->title.'|'.$book->price),
			'book_id'      => $book->id,
			'qty'     => 1,
        	'price'   => $book->price,
        	'name'    => $book->title,
        	'photo'    => $book->photo,
        	'author'=> $book->author,
		);
		$this->cart->insert($cart_list);
		$this->back_m->log('dodano produkty do koszyka'); 

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function remove_good($rowid) {
		$cart_list = array(
			'rowid'   => $rowid,
			'qty'     => 0
		);
		$this->cart->update($cart_list);
		$this->back_m->log('usunięto produkt z koszyka'); 
		redirect($_SERVER['HTTP_REFERER']);
	}
}

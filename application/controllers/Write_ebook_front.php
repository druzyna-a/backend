<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Write_ebook_front extends CI_Controller {

	public function book($id) {

            $data['contact'] = $this->back_m->get_one('contact_settings', 1);
            $data['settings'] = $this->back_m->get_one('settings', 1);
            $data['rows'] = $this->back_m->get_chapters('write_ebook', $id);
            
			echo loadViewsFront('list_chapters', $data);

	}

	public function form($type, $book, $id = '') {
			
            if($id != '') {
			    $data['value'] = $this->back_m->get_one('write_ebook', $id);
            }
            $data['contact'] = $this->back_m->get_one('contact_settings', 1);
            $data['settings'] = $this->back_m->get_one('settings', 1);
			echo loadViewsFront('form_chapters', $data);

	} 

	public function action($type, $table, $book, $id = '') {

			$this->form_validation->set_rules('chapter_title', 'Tytuł rozdziału', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('chapter_content', 'Treść rozdziału', 'min_length[2]|trim|required');

			$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
			$this->form_validation->set_message('required', 'Pole %s Jest wymagane');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('flashdata', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} else {

		
			foreach ($_POST as $key => $value) {

				if (!$this->db->field_exists($key, $table)) {
					$this->base_m->create_column($table, $key);
				}
				
					$insert[$key] = $value; 

			}
			$insert['book_id'] = $book; 
            if($type == 'insert') {
				$this->back_m->insert($table, $insert);
				$this->back_m->log('dodano nowy rozdział do książki '.$book.'');
			    $this->session->set_flashdata('flashdata', 'Rekord został dodany!');
            } else {
				$this->back_m->update($table, $insert, $id);
				$this->back_m->log('zaaktualizowano rozdział książki '. $book.' o id ' .$id.'');
			    $this->session->set_flashdata('flashdata', 'Rekord został zaktualizowany!');   
            }
			redirect('write_ebook_front/book/'.$book);
		}
    }
}
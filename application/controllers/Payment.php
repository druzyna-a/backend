<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	public function platnosc() {
        	require_once 'application/libraries/Przelewy24.php';
			$test = ($_POST["env"]==1?"1":"0");
			$salt = $_POST["salt"];
			$P24 = new Przelewy24($_POST["p24_merchant_id"],$_POST["p24_pos_id"],$salt,$test);
			foreach($_POST as $k=>$v) $P24->addValue($k,$v);                            
			file_put_contents("p24/parametry.txt","p24_crc=".$_POST['salt']."&p24_amount=".$_POST['p24_amount']."&p24_currency=".$_POST['p24_currency']."&env=".$test);
			
			$bool = ($_POST["redirect"]=="on")? true:false;
			$res = $P24->trnRegister($bool);

			if($res["error"]=='0') {
				$_POST['token'] = $res['token'];
				$_POST['p24_amount'] = $_POST['p24_amount'];
				$_POST['p24_currency'] = $_POST['p24_currency'];
				add_transaction($_POST);   
				redirect($P24->getHost()."trnRequest/".$res["token"]);
				exit;
			} else {
				echo $res['error'];
				exit;
			}
		
	}

	public function status() {
		$insert['payment'] = 1;
		$insert['status'] = 1;
		$this->back_m->update_payment('transaction', $insert, $_POST['p24_session_id']);
	}

	public function weryfikacja() {		
        require_once 'application/libraries/Przelewy24.php';
		if(file_exists ("p24/parametry.txt")){
			$result = file_get_contents("p24/parametry.txt");
			
			$X = explode("&", $result);
					
			foreach($X as $val) {
					$Y = explode("=", $val);
					$FIL[trim($Y[0])] = urldecode(trim($Y[1]));
						}
			
			$P24 = new Przelewy24($_POST["p24_merchant_id"],$_POST["p24_pos_id"],$FIL['p24_crc'],$FIL['env']);
			
			foreach($_POST as $k=>$v) $P24->addValue($k,$v);  
			
			$P24->addValue('p24_currency',$FIL['p24_currency']);
			$P24->addValue('p24_amount',$FIL['p24_amount']);
			$res = $P24->trnVerify();
			if(isset($res["error"]) and $res["error"] === '0'){
				$msg = 'Transakcja zosta�a zweryfikowana poprawnie';
				}
			else{
				$msg = 'B��dna weryfikacja transakcji';
			}
		}
		else{
			$msg = 'Brak pliku parametry.txt';
		}
		
		file_put_contents("p24/weryfikacja.txt",date("H:i:s").": ".$msg." \n\n",FILE_APPEND);

	}

	public function podsumowanie() {
		foreach ($this->cart->contents() as $value) {
			$cart = array(
			'rowid'   => $value['rowid'],
			'qty'     => 0
			);
			$this->cart->update($cart); 
		}
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		//redirect(base_url());
		//$data = loadDefaultFront();
		$this->back_m->log('dokonanie zakupu'); 
		echo loadViewsFront('payment_summary', $data);
	}
}

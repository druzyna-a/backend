<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (!$this->db->table_exists('users')){
			$this->base_m->create_base();
		}
		if(!isset($_SESSION['lang'])){
			$_SESSION['lang'] = 'pl';
			}
	}

	public function index() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['about'] = $this->back_m->get_all('about');
		$data['normal_user'] = $this->back_m->get_one('price_plan_desc', 1);
		$data['premium_user'] = $this->back_m->get_one('price_plan_desc', 2);
		$data['slider'] = $this->back_m->get_all('slider');
		$data['blog'] = $this->back_m->get_all('blog');
		$data['books'] = $this->back_m->get_all('books');
		echo loadViewsFront('index', $data);
	}

	public function payment() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);

		echo loadViewsFront('payment', $data);
	}

	public function cart() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);

		echo loadViewsFront('cart', $data);
	}

	public function about() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['about'] = $this->back_m->get_all('about');
		$data['normal_user'] = $this->back_m->get_one('price_plan_desc', 1);
		$data['premium_user'] = $this->back_m->get_one('price_plan_desc', 2);
		echo loadViewsFront('about', $data);
	}

	public function blog() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['blog'] = $this->back_m->get_all('blog');
		echo loadViewsFront('blog', $data);
	}

	public function profile() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['user_data'] = $this->back_m->get_one('klienci', $_SESSION['id']);
		echo loadViewsFront('edit_profile', $data);
	}

	public function contact() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		echo loadViewsFront('contact', $data);
	}

	public function entry($id) {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['entry'] = $this->back_m->get_one('blog', $id);
		echo loadViewsFront('entry', $data);
	}

	public function shop() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['categories'] = $this->back_m->get_all('books_categories');

		echo loadViewsFront('shop', $data);
	}

	public function shop_listing(){
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$kategoria = $this->input->get('kategoria');

		$data['books'] = $this->back_m->get_books_categories($kategoria);

		$this->load->view('front/pages/shop_listing',$data);
	}

	public function shop_product($id) {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['book'] = $this->back_m->get_one('books', $id);

		echo loadViewsFront('shop_product', $data);
	}

	public function list_blogs_entry() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['blogs_entry'] = $this->back_m->get_users_blogs_entry('blog', $_SESSION['id']);

		echo loadViewsFront('list_blog_entry', $data);
	}

	public function users_books_listing() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['user_ebooks'] = $this->back_m->get_all('add_ebook');

		echo loadViewsFront('users_ebooks_list', $data);
	}

	public function user_book($id) {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['user_ebook'] = $this->back_m->get_one('add_ebook', $id);

		echo loadViewsFront('user_ebook', $data);
	}

	public function read_user_book($id) {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['read_ebook'] = $this->back_m->get_chapters('write_ebook', $id);

		if(isset($_SESSION['login_client']) && $_SESSION['login_client'] == true) {
			echo loadViewsFront('read_ebook', $data);
		} else {
			echo loadViewsFront('read_ebook_not_logged', $data);
		}
	}

	public function user_profile_info($id) {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['user_data'] = $this->back_m->get_one('klienci', $id);
		$data['user_ebooks'] = $this->back_m->get_users_books('add_ebook', $id);

		echo loadViewsFront('user_profile_info', $data);
	}

	public function delete($table, $id) {

		$this->session->set_flashdata('flashdata', 'Rekord został usunięty!');
		$this->back_m->delete($table , $id);
		$this->back_m->log('usunięto rekord z tabeli ' .$table.' o id '.$id.''); 

		redirect($_SERVER['HTTP_REFERER']);

	}

	public function library() {
		$data['contact'] = $this->back_m->get_one('contact_settings', 1);
		$data['settings'] = $this->back_m->get_one('settings', 1);
		$data['user_transactions'] = $this->back_m->get_user_transactions('transaction', $_SESSION['id']);

		echo loadViewsFront('user_library', $data);
	}

	public function change($lang)
	{
		$_SESSION['lang'] = $lang;
	}

	public function create_pdf($id) {

        if(checkAccess($access_group = ['1'], $_SESSION['premium'])) {

        $data['user_ebook'] = $this->back_m->get_one('add_ebook', $id);

		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 0);
		require_once 'vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf();
        
        $mpdf->setFooter('{PAGENO}');

        $html=$this->load->view('front/pages/pdf_view/book_view', $data,true);  


		$mpdf->WriteHTML($html);
		ini_set('date.timezone', 'Europe/London');
        $now = date('Y-m-d-His');
        
        $mpdf->Output();
        $this->back_m->log('wygenerowano, oraz pobrano książke od id '.$id.''); 

    } else {
		redirect($_SERVER['HTTP_REFERER']);
	}
    }
	
}
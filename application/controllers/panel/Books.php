<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {

	public function category($id) {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
			if (!$this->db->table_exists($this->uri->segment(2))){
				$data['media'] = $this->back_m->create_table($this->uri->segment(2));
			}
            // DEFAULT DATA
			$data = loadDefaultData();

			$data['rows'] = $this->back_m->get_category($this->uri->segment(2), $id);
			echo loadSubViewsBack($this->uri->segment(2), 'index', $data);
		} else {
			redirect('panel');
		}
	}

	public function form($type, $category, $id = '') {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
            // DEFAULT DATA
			$data = loadDefaultData();
			
            if($id != '') {
			    $data['value'] = $this->back_m->get_one($this->uri->segment(2), $id);
            }
			echo loadSubViewsBack($this->uri->segment(2), 'form', $data);
		} else {
			redirect('panel');
		}
	} 

	public function action($type, $table, $category, $id = '') {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {

			$this->form_validation->set_rules('title', 'Tytuł wpisu', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('author', 'Autor', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('tags', 'Tagi', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('year', 'Rok wydania', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('publishing_house', 'Wydawnictwo', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('price', 'Cena', 'min_length[2]|trim|required');
			$this->form_validation->set_rules('description', 'Treść wpisu', 'min_length[2]|trim|required');

			$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
			$this->form_validation->set_message('required', 'Pole %s Jest wymagane');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('flashdata', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} else {

			$now = date('Y-m-d');
			if (!is_dir('uploads/'.$now)) {
				mkdir('./uploads/' . $now, 0777, TRUE);
			}
			$config['upload_path'] = './uploads/'.$now;
			$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|epub|mobi';
			$config['max_size'] = 0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			
			foreach ($_POST as $key => $value) {

				if (!$this->db->field_exists($key, $table)) {
					$this->base_m->create_column($table, $key);
				}

				if($key == 'name_photo_1') {
					if ($this->upload->do_upload('photo_1')) {
						$data = $this->upload->data();
						$insert['photo'] = $now.'/'.$data['file_name'];  
					} elseif($value == 'usunięte') {
						$insert['photo'] = '';
					}
				}
				if($key == 'name_file') {
					if ($this->upload->do_upload('file')) {
						$data = $this->upload->data();
						$insert['file'] = $now.'/'.$data['file_name'];  

					} elseif($value == 'usunięte') {
						$insert['file'] = '';
					}
				} else {
					$insert[$key] = $value; 
				}
            }
			$insert['category_id'] = $category; 
            if($type == 'insert') {
			    $this->back_m->insert($table, $insert);
			    $this->session->set_flashdata('flashdata', 'Rekord został dodany!');
            } else {
			    $this->back_m->update($table, $insert, $id);
			    $this->session->set_flashdata('flashdata', 'Rekord został zaktualizowany!');   
            }
			redirect('panel/'.$table.'/category/'.$category);
		}
		} else {
			redirect('panel');
		}
    }
}
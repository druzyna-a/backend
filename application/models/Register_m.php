<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_m extends CI_Model  
{
    public function register($data) {
        $insert['special_key'] = md5($data['email']);
        $insert['email'] = $data['email'];
        $insert['password'] = hashPassword($data['password']);
        $this->back_m->insert('klienci', $insert);
        if(send_active_mail($insert['email'],$insert['special_key'])) {
            return true;
        } else {
            return false;
        }
    }

    public function check_email($email) {
        $data['users'] = $this->back_m->get_all('users');
		foreach($data['users'] as $check) {
			if($email == $check->email && $check->active == '1') {
				if(send_new_password($check->id, $check->email)){
                    return true;
                } else {
                    return false;
                }
                break;
            } else {
                $this->session->set_flashdata('flashdata', 'Błędny adres E-mail lub Twoje konto nie zostało aktywowane');
				return false;
            }
        }
    }
}

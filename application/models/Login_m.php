<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends CI_Model  
{
    public function check_login($login, $password) {
        $data['users'] = $this->back_m->get_all('users');
        
		foreach($data['users'] as $check) {
			if(($login == $check->login || $login == $check->email) && (password_verify($password, $check->password)) &&$check->active == '1') {
				$session_data['id'] = $check->id;
				$session_data['first_name'] = $check->first_name;
				$session_data['last_name'] = $check->last_name;
				$session_data['email'] = $check->email;
				$session_data['name'] = $check->login;
				$session_data['rola'] = $check->rola;
				$session_data['login'] = TRUE;
				$this->session->set_userdata($session_data);
				$logged = true;
                break;
            } else {
				$logged = false;
            }
        }
            
		if($logged == false) {
			$this->session->set_flashdata('flashdata', 'Błędny login lub hasło lub konto nie zostało aktywowane');
            return false;
		} else {
			$this->session->set_flashdata('flashdata', 'Zostałeś poprawnie zalogowany');
            return true;
        }
    }

    public function check_client($login, $password) {
        $data['users'] = $this->back_m->get_all('klienci');
        
		foreach($data['users'] as $check) {
			if($login == $check->email && (password_verify($password, $check->password)) && $check->active == '1') {
				$session_data['id'] = $check->id;
				$session_data['photo'] = $check->photo;
				$session_data['first_name'] = $check->first_name;
				$session_data['last_name'] = $check->last_name;
				$session_data['email'] = $check->email;
				$session_data['name'] = $check->login;
				$session_data['state'] = $check->state;
				$session_data['phone'] = $check->phone;
				$session_data['address'] = $check->address;
				$session_data['zip_code'] = $check->zip_code;
				$session_data['city'] = $check->city;
				$session_data['country'] = $check->country;
				$session_data['company'] = $check->company;
				$session_data['nip'] = $check->nip;
				$session_data['zip_company'] = $check->zip_company;
				$session_data['city_company'] = $check->city_company;
				$session_data['address_company'] = $check->address_company;
				$session_data['country_company'] = $check->country_company;
				$session_data['premium'] = $check->premium;
				$session_data['description'] = $check->descripion;
				$session_data['login_client'] = TRUE;
				$this->session->set_userdata($session_data);
				$logged = true;
                break;
            } else {
				$logged = false;
            }
        }
            
		if($logged == false) {
			$this->session->set_flashdata('flashdata', 'Błędny login lub hasło lub konto nie zostało aktywowane');
            return false;
		} else {
			$this->session->set_flashdata('flashdata_success', 'Zostałeś poprawnie zalogowany');
            return true;
        }
	}
	
	public function check_email_client($email) {
        $data['users'] = $this->back_m->get_all('klienci');
		foreach($data['users'] as $check) {
			if($email == $check->email && $check->active == '1') {
				if(send_new_password_users($check->id, $check->email)){
                    return true;
                } else {
                    return false;
                }
                break;
            } else {
                $this->session->set_flashdata('flashdata', 'Błędny adres E-mail lub Twoje konto nie zostało aktywowane');
				return false;
            }
        }
    }

    public function check_email($email) {
        $data['users'] = $this->back_m->get_all('users');
		foreach($data['users'] as $check) {
			if($email == $check->email && $check->active == '1') {
				if(send_new_password($check->id, $check->email)){
                    return true;
                } else {
                    return false;
                }
                break;
            } else {
                $this->session->set_flashdata('flashdata', 'Błędny adres E-mail lub Twoje konto nie zostało aktywowane');
				return false;
            }
        }
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Back_m extends CI_Model  
{
    public function create_table($table) {
	    return $this->db->query('CREATE TABLE '.$table.' (
	    	id int NOT NULL AUTO_INCREMENT,
	    	created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	    	title TEXT,
	    	subtitle TEXT,
	    	description TEXT,
	    	photo TEXT,
	    	alt TEXT,
	    	PRIMARY KEY(id)
	    );');  
    }
    
    public function get_category($table, $id) {
        $this->db->where(['category_id' => $id]);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_chapters($table, $id) {
        $this->db->where(['book_id' => $id]);
        $query = $this->db->get($table);
        return $query->result();
    }


    public function get_category_front($table, $id) {
        $this->db->where(['category_id' => $id]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_all($table) {
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_one($table, $id) {
        $this->db->where(['id' => $id]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_images($table, $table_name, $id) {
        $this->db->where([
            'item_id' => $id,
            'table_name' => $table_name,
            ]);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function insert($table, $data) {
        $data = $this->security->xss_clean($data);
        $query = $this->db->insert($table, $data);
        return $query;
    }

    public function update($table, $data, $id) {
        $data = $this->security->xss_clean($data);
        $this->db->where(['id' => $id]);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function delete($table, $id) {
        $this->db->where(['id' => $id]);
        $query = $this->db->delete($table);
        return $query;
    }

    public function delete_comment($table, $id) {
        $this->db->where(['comments_id' => $id]);
        $query = $this->db->delete($table);
        return $query;
    }

    public function get_users_blogs_entry($table, $user_id) {
        $this->db->where(['user_id' => $user_id]);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_users_books($table, $user_id) {
        $this->db->where(['user_id' => $user_id]);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_page($table, $page) {
        $this->db->where(['page' => $page]);
        $query = $this->db->get($table);
        return $query->row();
    }
    
    public function get_gallery($table,$item_id) {
        $this->db->where(['table_name' => $table]);
        $this->db->where(['item_id' => $item_id]);
        $query = $this->db->get('gallery');
        
        return $query->result();        
    }

    public function get_user_data($user_id) {
        $this->db->where(['id' => $user_id]);
        $query = $this->db->get('klienci');
        return $query->row();
    }

    public function log($typ) {
        if($_SESSION['id']){
            $data['user_id'] = $_SESSION['id'];
            $data['typ'] = $typ;
            $insert_query = $this->db->insert('logs', $data);
            return $insert_query;
        }

    }
    public function update_account($table, $data, $special_key) {
        $this->db->where(['special_key' => $special_key]);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function update_payment($table, $data, $token) {
        $this->db->where(['p24_session_id' => $token]);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function update_premium($table, $data, $token) {
        $this->db->where(['session_id' => $token]);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function update_sessionId($table, $data, $id) {
        $this->db->where(['id' => $id]);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function get_user_transactions($table, $id) {
        $this->db->where(['status' => '1']);
        $this->db->where(['user_id' => $id]);
        $query = $this->db->get($table);
        return $query->result();  
    }

    public function get_books_categories($kategoria)
    {   

        if($kategoria && $kategoria != 'none'):
            $this->db->where(['category_id' => $kategoria]);
        endif;

        $query = $this->db->get('books');

        return $query->result();            
    }


}
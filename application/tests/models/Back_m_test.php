<?php

class Back_m_test extends TestCase
{

  public function setUp()
    {
      $this->resetInstance();
      $this->CI->load->model('Back_m');
      $this->obj = $this->CI->Back_m;
    }





    public function test_get_category()
      {
        $expected =
          [
    			  5 => 'Test',
				  8 => 'Wiedźmin',
				  9 => 'Sword Art Online 01',
				  11 => 'test'
    		  ];
    		$list = $this->obj->get_category('books','4');
        if (!$list)
          {
            $this->fail('!!!!!!!!!!!!!!!!!!!! Query result is empty! !!!!!!!!!!!!!!!!!!!!');
          } else
        {
      		foreach ($list as $book)
          {
      		    $this->assertEquals($expected[$book->id], $book->title);
          }
        }
      }

      public function test_get_category_front()
        {
          $actual = $this->obj->get_category_front('books','4');
          $expected =
            [
              5 => 'Test'
            ];
          $this->assertEquals($expected[$actual->id], $actual->title);
        }

        public function test_get_images()
          {
            $expected =
              [
        			  1 => '2019-11-25/1.png',
                2 => '2019-11-25/2.jpg',
                3 => '2019-11-25/3.jpg',
        		  ];
        		$list = $this->obj->get_images('gallery','blog','6');
            if (!$list)
              {
                $this->fail('!!!!!!!!!!!!!!!!!!!! Query result is empty! !!!!!!!!!!!!!!!!!!!!');
              } else
            {
          		foreach ($list as $image)
              {
          		    $this->assertEquals($expected[$image->id], $image->photo);
              }
            }
          }

      public function test_get_all()
        {
          $expected =
            [
      			  2 => 'Horror',
      			  4 => 'Fantastyka',
				107 => 'Light Novel'
      		  ];
      		$list = $this->obj->get_all('books_categories');
      		foreach ($list as $category)
          {
      		    $this->assertEquals($expected[$category->id], $category->category_name);
          }
        }

    public function test_get_one()
    {
      $actual = $this->obj->get_one('books_categories','4');
      $expected =
        [
    			4 => 'Fantastyka',
    		];
    	$this->assertEquals($expected[$actual->id], $actual->category_name);
    }

    public function test_insert()
    {
      $data =
      [
        'id' => '100',
        'category_name' => 'TestCategory',
      ];
      $this->obj->insert('books_categories', $data);

      $expected =
      [
        100 => 'TestCategory',
      ];
      $actual = $this->obj->get_one('books_categories','100');
      $this->assertEquals($expected[$actual->id], $actual->category_name);
    }

    public function test_update()
    {
      $data =
      [
        'category_name' => 'TestCategoryUpdated',
      ];
      $expected =
      [
        100 => 'TestCategoryUpdated'
      ];
      $this->obj->update('books_categories', $data, '100');
      $actual = $this->obj->get_one('books_categories','100');
      $this->assertEquals($expected[$actual->id], $actual->category_name);
    }

    public function test_delete()
    {
      $notexpected =
      [
        100 => 'TestCategoryUpdated'
      ];
      $this->obj->delete('books_categories', '100');
      $list = $this->obj->get_all('books_categories');
      foreach ($list as $category)
      {
        $this->assertNotEquals(@$notexpected[$category->id], $category->category_name);
      }
    }

    public function test_get_gallery()
    {
      $expected =
        [
          1 => '2019-11-25/1.png',
          2 => '2019-11-25/2.jpg',
          3 => '2019-11-25/3.jpg',
        ];
        $list = $this->obj->get_gallery('blog','6');
        if (!$list)
          {
            $this->fail('!!!!!!!!!!!!!!!!!!!! Query result is empty! !!!!!!!!!!!!!!!!!!!!');
          } else
        {
          foreach ($list as $image)
          {
              $this->assertEquals($expected[$image->id], $image->photo);
          }
        }
    }

}

<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

class Login_m_test extends TestCase
{

  public function setUp()
    {
      $this->resetInstance();
      $this->CI->load->model('Login_m');
      $this->obj = $this->CI->Login_m;
    }





    public function test_check_login()
    {
      $check_login_fails = $this->obj->check_login('admin','2uptgKit1');
      $this->assertEquals(false, $check_login_fails);
      $check_login_fails = $this->obj->check_login('aadmin','3uptgKit1');
      $this->assertEquals(false, $check_login_fails);
      $check_login_fails = $this->obj->check_login('ADMIN','3uptgKit1');
      $this->assertEquals(false, $check_login_fails);
      $check_login_fails = $this->obj->check_login('admin','3UPTGKIT1');
      $this->assertEquals(false, $check_login_fails);

      $check_login = $this->obj->check_login('admin','3uptgKit1');
      $this->assertEquals(true, $check_login);
    }

    public function test_check_client()
    {
      $check_client_fails = $this->obj->check_client('pab17065@zzrgg.com','zl386Fsiw#dk');
      $this->assertEquals(false, $check_client_fails);
      $check_client_fails = $this->obj->check_client('pab27065@zzrgg.com','zl286Fsiw#dk');
      $this->assertEquals(false, $check_client_fails);
      $check_client_fails = $this->obj->check_client('PAB27065@ZZRGG.COM','zl386Fsiw#dk');
      $this->assertEquals(false, $check_client_fails);
      $check_client_fails = $this->obj->check_client('pab27065@zzrgg.com','ZL386FSIW#DK');
      $this->assertEquals(false, $check_client_fails);

      $check_client = $this->obj->check_client('pab27065@zzrgg.com','zl386Fsiw#dk');
      $this->assertEquals(true, $check_client);
    }

}

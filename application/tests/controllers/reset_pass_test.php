<?php

class reset_pass_test extends TestCase
{
	public function setUp()
    {
        // Load CI instance normally
        $this->resetInstance();
    }

	public function test_reset_pass_view()
    {
        $output = $this->request('GET', 'panel/Home/reset_pass');
        $this->assertContains('<button type="submit" class="btn btn-info btn-block">Zresetuj hasło</button>', $output);
    }

}

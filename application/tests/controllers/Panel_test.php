<?php

if (!isset($_SESSION)) $_SESSION = array();

class Panel_test extends TestCase
{
	public static $shared_session = array();
	protected $backupGlobalsBlacklist = array( '_SESSION' );




	public function setUp()
    {
			$_SESSION = Panel_test::$shared_session;
      $this->resetInstance();
    }

	public function tearDown()
		{
    	Panel_test::$shared_session = $_SESSION;
    }



	public function test_session()
		{
			$_SESSION['foo'] = 'bar';
		  $this->assertEquals('bar', $_SESSION['foo']);
		}

	public function test_login()
		{
			$output = $this->request('GET', 'panel/Home/index');
			$this->assertContains('<input type="text" class="form-control" name="login" placeholder="Wprowadź login lub adres e-mail">', $output);
			$this->request('POST', 'panel/Home/index', ['login' => 'admin', 'password' => '3uptgKit1']);
		}

	public function test_blog()
    {
			$output = $this->request('GET', 'panel/Blog/index');
			$this->assertContains('<h4 class="tx-gray-800 mg-b-5">Blog</h4>', $output);
    }

	public function test_books_categories()
    {
			$output = $this->request('GET', 'panel/Books_categories/index');
			$this->assertContains('<h4 class="tx-gray-800 mg-b-5">Books categories</h4>', $output);
			$this->assertContains('Horror', $output);
			$this->assertContains('Fantastyka', $output);
    }

	public function test_profile()
    {
			$output = $this->request('GET', 'panel/Profile/index');
			$this->assertContains('<h4 class="tx-gray-800 mg-b-5">Profil</h4>', $output);
    }

	public function test_about()
	  {
			$output = $this->request('GET', 'panel/About/index');
			$this->assertContains('<h4 class="tx-gray-800 mg-b-5">About</h4>', $output);
	  }

	public function test_dashboard()
    {
			$output = $this->request('GET', 'panel/Dashboard/index');
			$this->assertContains('<h4 class="tx-gray-800 mg-b-5">Dashboard</h4>', $output);
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['o_nas'] = 'home/about';
$route['blog'] = 'home/blog';
$route['blog_wpis/(.*)/(.+)'] = 'home/entry/$1/$2';
$route['profil'] = 'home/profile';
$route['sklep'] = 'home/shop';
$route['ksiazki_uzytkownikow'] = 'home/users_books_listing';
$route['kontakt'] = 'home/contact';
$route['ksiazka/(.*)/(.+)'] = 'home/shop_product/$1/$2';
$route['ksiazka_uzytkownika/(.*)/(.+)'] = 'home/user_book/$1/$2';
$route['czytaj/(.*)/(.+)'] = 'home/read_user_book/$1/$2';
$route['pobierz_pdf/(.*)/(.+)'] = 'home/create_pdf/$1/$2';
$route['twoje_wpisy'] = 'blog_front';
$route['twoje_ksiazki'] = 'add_ebook_front';
$route['biblioteka'] = 'home/library';
$route['ksiazki_uzytkownikow'] = 'home/users_books_listing';
$route['dodaj_wpis/form/insert'] = 'blog_front/form/insert';
$route['edytuj_wpis/form/update/(.*)/(.+)'] = 'blog_front/form/update/$1/$2';
$route['dodaj_ksiazke/form/insert'] = 'add_ebook_front/form/insert';
$route['edytuj_ksiazke/form/update/(.*)/(.+)'] = 'add_ebook_front/form/update/$1/$2';
$route['rozdzialy/(.*)/(.+)'] = 'write_ebook_front/book/$1/$2';
$route['dodaj_nowy_rozdzial/form/insert/(.*)/(.+)'] = 'write_ebook_front/form/insert/$1/$2';
$route['edytuj_rozdzial/form/update/(.*)/(.+)'] = 'write_ebook_front/form/update/$1/$2';
$route['profil_uzytkownika/(.*)'] = 'home/user_profile_info/$1';



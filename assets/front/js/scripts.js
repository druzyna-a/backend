$('#contact-form').submit(function(event) {
    event.preventDefault();
    var email = $('#email').val();

    grecaptcha.ready(function() {
        grecaptcha.execute('6LcdRMwUAAAAABCvy21DWoEhiXG0cZxpapcG1bss', {action: 'mailer/send'}).then(function(token) {
            $('#contact-form').prepend('<input type="hidden" name="token" value="' + token + '">');
            $('#contact-form').prepend('<input type="hidden" name="secret_key" value="6LcdRMwUAAAAAMD9I9kmDKCTKU_736EryDBoF3lH">');
            $('#contact-form').prepend('<input type="hidden" name="action" value="mailer/send">');
            $('#contact-form').unbind('submit').submit();
        });;
    });
});


function sortTableData(n) {
    var table, rows, switching, i, shouldSwitch, dir, switchcount = 0;
    var x = 0;
    var y = 0;
    table = document.getElementById("filtrableTable");
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc"; 
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /*Loop through all table rows (except the
      first, which contains table headers):*/
      for (i = 1; i < (rows.length - 1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        /*Get the two elements you want to compare,
        one from current row and one from the next:*/
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        x = Date.parse(x.innerHTML.toLowerCase());
        y = Date.parse(y.innerHTML.toLowerCase());
        /*check if the two rows should switch place,
        based on the direction, asc or desc:*/
        if (dir == "asc") {
          if (x > y) {
            //if so, mark as a switch and break the loop:
            shouldSwitch= true;
            break;
          }
        } else if (dir == "desc") {
          if (x < y) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        /*If a switch has been marked, make the switch
        and mark that a switch has been done:*/
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        //Each time a switch is done, increase this count by 1:
        switchcount ++;      
      } else {
        /*If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again.*/
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }

  function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("filtrableTable");
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc"; 
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 1; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        /* Check if the two rows should switch place,
        based on the direction, asc or desc: */
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        // Each time a switch is done, increase this count by 1:
        switchcount ++; 
      } else {
        /* If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again. */
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }

  function searchFiltr(nr) {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("searchInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("filtrableTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[nr];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }       
    }
  }

  $('#hideInfo').delay(3000).fadeOut(1000);


  new WOW().init();

  var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

      function moveBackground() {
        x += (lFollowX - x) * friction;
        y += (lFollowY - y) * friction;
        
        translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

        $('.moving__photo').css({
          '-webit-transform': translate,
          '-moz-transform': translate,
          'transform': translate
        });

        window.requestAnimationFrame(moveBackground);
      }

      $(".view").on('mousemove click', function(e) {

        var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 6 - e.clientX));
        var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 6 - e.clientY));
        lFollowX = (40 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
        lFollowY = (30 * lMouseY) / 100;

      });

      $(document).ready(function() {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 50,
            nav: true,
            loop: true,
            responsive: {
                0: {
                    items: 1
                },
                750: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        })
    })

    $('.carousel').carousel({
        interval: 2000
        })


        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
              "palette": {
                "popup": {
                  "background": "#000",
                  "text": "#fff"
                },
                "button": {
                  "background": "#fff",
                  "text": "#000"
                }
              },
              "type": "opt-out",
              "content": {
                "message": "Nasza strona internetowa korzysta z plików cookie. Dzięki temu możemy zapewnić naszym użytkownikom satysfakcjonujące wrażenia z przeglądania naszej witryny i jej prawidłowe funkcjonowanie.",
                "dismiss": "Rozumiem",
                "deny": "",
                "allow": "Rozumiem",
                "link": "Czytaj więcej...",
                "href": "<?php echo base_url(); ?>uploads/<?php echo $settings->privace ?>"
              }
            })});



            tinymce.init({
                selector:'.summernote',
                paste_as_text: true,
                plugins : 'advlist autolink link image lists charmap print preview code paste',
                toolbar: 'undo redo | formatselect | ' +
                    ' bold italic backcolor | alignleft aligncenter ' +
                    ' alignright alignjustify | bullist numlist outdent indent |' +
                    ' removeformat | help',
                advlist_bullet_styles: "square",
                valid_elements : '*[*]',
                height: 600,
                setup: function (editor) {
                    editor.on('change', function (e) {
                        editor.save();
                    });
                }
            });